import React, { useState, useEffect } from 'react';
import {
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TablePagination,
	TableRow,
	Button,
	Typography,
	Dialog,
	DialogTitle,
	DialogContent,
	DialogActions,
	TextField,
	MenuItem,
	Grid,
	Checkbox,
} from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import CustomTableHead from './tableHead';
import { useStyles } from '../../../styles/customTable';
import moment from 'moment';

function descendingComparator(a, b, orderBy) {
	if (b[orderBy] < a[orderBy]) {
		return -1;
	}
	if (b[orderBy] > a[orderBy]) {
		return 1;
	}
	return 0;
}

function getComparator(order, orderBy) {
	return order === 'desc'
		? (a, b) => descendingComparator(a, b, orderBy)
		: (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
	const stabilizedThis = array.map((el, index) => [el, index]);
	stabilizedThis.sort((a, b) => {
		const order = comparator(a[0], b[0]);
		if (order !== 0) return order;
		return a[1] - b[1];
	});
	return stabilizedThis.map((el) => el[0]);
}

const CustomDataTable = ({
	rows,
	headCells,
	params,
	token,
	createRecord,
	updateRecord,
	deleteRecord,
	resetFlags,
	isRecordCreated,
	isRecordUpdated,
	isRecordDeleted,
	runOperators,
}) => {
	const classes = useStyles();
	const [order, setOrder] = useState('asc');
	const [orderBy, setOrderBy] = useState('date');
	const [page, setPage] = useState(0);
	const [dense, setDense] = useState(false);
	const [rowsPerPage, setRowsPerPage] = useState(5);
	const [addModal, setAddModal] = useState('');
	const [deleteModal, setDeleteModal] = useState(false);
	const [selectedid, setselectedid] = useState('');
	const [addRecordFields, setAddRecordFields] = useState({});
	const [temporalOp, settemporalOp] = useState('');
	const [selected, setSelected] = useState([]);
	const [evolutionHistory, showEvolutionHistory] = useState(false);
	const [CurrentEvolution, showCurrentEvolution] = useState(false);
	const [firstLastEvoltion, showFirstLastEvoltion] = useState(false);
	const [showPrevious, setShowPrevious] = useState(false);
	const [showNext, setShowNext] = useState(false);
	const [showPreviousScale, setShowPreviousScale] = useState(false);
	const [showNextScale, setShowNextScale] = useState(false);
	const [previousScaleColumn, setPreviousScaleColumn] = useState('');
	const [nextScaleColumn, setNextScaleColumn] = useState('');
	const [previousScaleValue, setPreviousScaleValue] = useState('');
	const [nextScaleValue, setNextScaleValue] = useState('');
	const [showColumnValue, setShowColumnValue] = useState(false);
	const [showColumnDate, setShowColumnDate] = useState(false);
	const [selectedDate, setSelectedDate] = useState('');
	const [evolutionVal, setEvolutionVal] = useState(false);
	const [columnTimestamp, setColumnTimestamp] = useState(false);

	useEffect(() => {
		if (isRecordCreated || isRecordUpdated || isRecordDeleted) {
			setAddModal(false);
			setDeleteModal(false);
			resetFlags();
		}
	}, [isRecordCreated || isRecordUpdated || isRecordDeleted]);

	const handleDelete = (id) => {
		setselectedid(id);
		setDeleteModal(true);
	};

	const handleDeleteRecord = () => {
		deleteRecord({
			databaseName: params.databaseName,
			tableName: params.tableName,
			id: selectedid,
		});
	};

	const handleCreate = (value, id) => {
		let temp = {};
		if (id) {
			headCells.length &&
				headCells.length > 0 &&
				headCells.map((item) => {
					const a = rows.find((item) => item.id === id);

					temp = {
						...temp,
						[item.label]: a[item.label],
					};
				});
		} else {
			headCells.length &&
				headCells.length > 0 &&
				headCells.map((item) => {
					temp = { ...temp, [item.label]: '' };
				});
		}

		setAddModal(value);
		setAddRecordFields(temp);
	};

	const handleClose = () => {
		setAddModal(false);
		setDeleteModal(false);
	};

	const handleCreateRecord = () => {
		const { id, ...rest } = addRecordFields;
		createRecord({
			databaseName: params.databaseName,
			tableName: params.tableName,
			...rest,
		});
	};

	const handleEditRecord = () => {
		updateRecord({
			databaseName: params.databaseName,
			tableName: params.tableName,
			...addRecordFields,
		});
	};

	const handleRequestSort = (event, property) => {
		const isAsc = orderBy === property && order === 'asc';
		setOrder(isAsc ? 'desc' : 'asc');
		setOrderBy(property);
	};

	const handleChangePage = (event, newPage) => {
		setPage(newPage);
	};

	const handleChangeRowsPerPage = (event) => {
		setRowsPerPage(parseInt(event.target.value, 10));
		setPage(0);
	};

	const handleChangeDense = (event) => {
		setDense(event.target.checked);
	};

	const getFirst = (value) => {
		settemporalOp(value);
	};

	const handleClickCheckbox = (e, id) => {
		e.preventDefault();
		e.stopPropagation();
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(
				selected.slice(0, selectedIndex),
				selected.slice(selectedIndex + 1)
			);
		}

		setSelected(newSelected);
	};

	const getOperatorValue = (value) => {
		runOperators(value);
	};

	const getEvolutionHistory = () => {
		console.log(selected);
	};
	console.log(selected, headCells);
	const isSelected = (id) => selected.indexOf(id) !== -1;

	const emptyRows =
		rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
	return (
		<div className={classes.root}>
			<Paper className={classes.paper}>
				{params && params.tableName.search('_hist') !== -1 ? null : (
					<Button
						aria-controls="customized-menu"
						aria-haspopup="true"
						variant="contained"
						color="primary"
						onClick={() => handleCreate('add')}
						style={{ float: 'right', margin: '20px' }}
					>
						Create New Record
					</Button>
				)}
				<TableContainer>
					<Table
						className={classes.table}
						aria-labelledby="tableTitle"
						size={dense ? 'small' : 'medium'}
						aria-label="enhanced table"
					>
						<CustomTableHead
							order={order}
							orderBy={orderBy}
							onRequestSort={handleRequestSort}
							rowCount={rows.length}
							headCells={headCells}
							checkbox={true}
						/>

						<TableBody>
							{rows.length === 0 ? (
								<Typography
									variant="body1"
									style={{ padding: '20px', width: '200px' }}
								>
									No records found
								</Typography>
							) : null}
							{stableSort(rows, getComparator(order, orderBy))
								.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
								.map((row, index) => {
									const isItemSelected = isSelected(row.hist_id);
									const labelId = `enhanced-table-checkbox-${index}`;
									return (
										<TableRow hover key={row.id}>
											{params && params.tableName.search('_hist') !== -1 ? (
												<TableCell padding="checkbox">
													<Checkbox
														checked={isItemSelected}
														inputProps={{ 'aria-labelledby': labelId }}
														onClick={(e) => handleClickCheckbox(e, row.hist_id)}
													/>
												</TableCell>
											) : null}
											{headCells.length > 0
												? headCells.map((cell) => (
														<TableCell component="th" id={cell.id} scope="row">
															{row[cell.id]}
														</TableCell>
												  ))
												: null}
											{params &&
											params.tableName.search('_hist') !== -1 ? null : (
												<TableCell>
													<Button
														aria-controls="customized-menu"
														aria-haspopup="true"
														variant="text"
														onClick={() => handleCreate('edit', row.id)}
													>
														Edit
													</Button>
												</TableCell>
											)}
											{params &&
											params.tableName.search('_hist') !== -1 ? null : (
												<TableCell>
													<Button
														aria-controls="customized-menu"
														aria-haspopup="true"
														variant="text"
														onClick={() => handleDelete(row.id)}
													>
														Delete
													</Button>
												</TableCell>
											)}
										</TableRow>
									);
								})}
							{emptyRows > 0 && (
								<TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
									<TableCell colSpan={6} />
								</TableRow>
							)}
						</TableBody>
					</Table>
				</TableContainer>
				<TablePagination
					rowsPerPageOptions={[5, 10, 25]}
					component="div"
					count={rows.length}
					rowsPerPage={rowsPerPage}
					page={page}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</Paper>

			<FormControlLabel
				control={<Switch checked={dense} onChange={handleChangeDense} />}
				label="Dense padding"
			/>
			{params && params.tableName.search('_hist') !== -1 ? (
				<>
					<Grid container spacing={2}>
						<Grid item>
							<Button
								color="primary"
								variant="contained"
								// onClick={() => getFirst('first')}
							>
								Get First
							</Button>
							{/* {temporalOp === 'first' && */}
							{stableSort(rows, getComparator(order, orderBy)).map(
								(row, index) => {
									if (index === 0) {
										return (
											<Paper className={classes.paper}>
												<TableContainer>
													<Table
														className={classes.table}
														aria-labelledby="tableTitle"
														size={dense ? 'small' : 'medium'}
														aria-label="enhanced table"
													>
														<CustomTableHead
															order={order}
															orderBy={orderBy}
															onRequestSort={handleRequestSort}
															rowCount={rows.length}
															headCells={headCells}
															checkbox={false}
														/>

														<TableBody>
															<TableRow hover key={row.id}>
																{headCells.length > 0
																	? headCells.map((cell) => (
																			<TableCell
																				component="th"
																				id={cell.id}
																				scope="row"
																			>
																				{row[cell.id]}
																			</TableCell>
																	  ))
																	: null}
															</TableRow>
														</TableBody>
													</Table>
												</TableContainer>
											</Paper>
										);
									}
								}
							)}
						</Grid>
					</Grid>
					<Grid container spacing={2}>
						<Grid item>
							<Button
								color="primary"
								variant="contained"
								// onClick={() => getLast('last')}
							>
								Get Last
							</Button>
							{/* {temporalOp === 'last' && */}
							{stableSort(rows, getComparator(order, orderBy)).map(
								(row, index) => {
									if (index === rows.length - 1) {
										return (
											<Paper className={classes.paper}>
												<TableContainer>
													<Table
														className={classes.table}
														aria-labelledby="tableTitle"
														size={dense ? 'small' : 'medium'}
														aria-label="enhanced table"
													>
														<CustomTableHead
															order={order}
															orderBy={orderBy}
															onRequestSort={handleRequestSort}
															rowCount={rows.length}
															headCells={headCells}
															checkbox={false}
														/>

														<TableBody>
															<TableRow hover key={row.id}>
																{headCells.length > 0
																	? headCells.map((cell) => (
																			<TableCell
																				component="th"
																				id={cell.id}
																				scope="row"
																			>
																				{row[cell.id]}
																			</TableCell>
																	  ))
																	: null}
															</TableRow>
														</TableBody>
													</Table>
												</TableContainer>
											</Paper>
										);
									}
								}
							)}
						</Grid>
						<Grid container spacing={2}>
							<Grid item>
								<Button
									color="primary"
									variant="contained"
									onClick={() => setShowPrevious(!showPrevious)}
								>
									Get Previous
								</Button>
								{showPrevious ? (
									<Paper className={classes.paper}>
										<TableContainer>
											<Table
												className={classes.table}
												aria-labelledby="tableTitle"
												size={dense ? 'small' : 'medium'}
												aria-label="enhanced table"
											>
												<CustomTableHead
													order={order}
													orderBy={orderBy}
													onRequestSort={handleRequestSort}
													rowCount={rows.length}
													headCells={headCells}
													checkbox={false}
												/>

												<TableBody>
													{stableSort(rows, getComparator(order, orderBy)).map(
														(row, index) => {
															const id = rows.find(
																(item) => item.hist_id === selected[0]
															)?.id;

															if (
																row.hist_id < selected[0] &&
																row.hist_id >= selected[0] - 1 &&
																row.id === id
															) {
																return (
																	<TableRow hover key={row.id}>
																		{headCells.length > 0
																			? headCells.map((cell) => {
																					return (
																						<TableCell
																							component="th"
																							id={cell.id}
																							scope="row"
																						>
																							{row[cell.id]
																								? row[cell.id]
																								: 'Null'}
																						</TableCell>
																					);
																			  })
																			: null}
																	</TableRow>
																);
															}
														}
													)}
												</TableBody>
											</Table>
										</TableContainer>
									</Paper>
								) : null}
							</Grid>
						</Grid>
						<Grid container spacing={2}>
							<Grid item>
								<Button
									color="primary"
									variant="contained"
									onClick={() => setShowNext(!showNext)}
								>
									Get Next
								</Button>
								{showNext ? (
									<Paper className={classes.paper}>
										<TableContainer>
											<Table
												className={classes.table}
												aria-labelledby="tableTitle"
												size={dense ? 'small' : 'medium'}
												aria-label="enhanced table"
											>
												<CustomTableHead
													order={order}
													orderBy={orderBy}
													onRequestSort={handleRequestSort}
													rowCount={rows.length}
													headCells={headCells}
													checkbox={false}
												/>

												<TableBody>
													{stableSort(rows, getComparator(order, orderBy)).map(
														(row, index) => {
															const id = rows.find(
																(item) => item.hist_id === selected[0]
															)?.id;

															if (
																row.hist_id > selected[0] &&
																row.hist_id <= selected[0] + 1 &&
																row.id === id
															) {
																return (
																	<TableRow hover key={row.id}>
																		{headCells.length > 0
																			? headCells.map((cell) => {
																					return (
																						<TableCell
																							component="th"
																							id={cell.id}
																							scope="row"
																						>
																							{row[cell.id]
																								? row[cell.id]
																								: 'Null'}
																						</TableCell>
																					);
																			  })
																			: null}
																	</TableRow>
																);
															}
														}
													)}
												</TableBody>
											</Table>
										</TableContainer>
									</Paper>
								) : null}
							</Grid>
						</Grid>

						<Grid container spacing={2}>
							<Grid item>
								<Grid container direction="row">
									<Grid item>
										<Button
											color="primary"
											variant="contained"
											onClick={() => setShowPreviousScale(!showPreviousScale)}
										>
											Get Previous Scale
										</Button>
									</Grid>
									<Grid item>
										<TextField
											id="standard-basic"
											label="Column Name"
											variant="outlined"
											style={{
												width: '100%',
												marginBottom: '10px',
												paddingBottom: '2px',
												marginLeft: '15px',
											}}
											value={previousScaleColumn}
											onChange={(e) => setPreviousScaleColumn(e.target.value)}
											// select
										/>
									</Grid>
									<Grid item>
										<TextField
											id="standard-basic"
											label="Value"
											variant="outlined"
											style={{
												width: '100%',
												marginBottom: '10px',
												paddingBottom: '2px',
												marginLeft: '15px',
											}}
											value={previousScaleValue}
											onChange={(e) => setPreviousScaleValue(e.target.value)}
											// select
										/>
									</Grid>
								</Grid>
								{showPreviousScale ? (
									<Paper className={classes.paper}>
										<TableContainer>
											<Table
												className={classes.table}
												aria-labelledby="tableTitle"
												size={dense ? 'small' : 'medium'}
												aria-label="enhanced table"
											>
												<CustomTableHead
													order={order}
													orderBy={orderBy}
													onRequestSort={handleRequestSort}
													rowCount={rows.length}
													headCells={headCells}
													checkbox={false}
												/>

												<TableBody>
													{stableSort(rows, getComparator(order, orderBy)).map(
														(row, index) => {
															const id = rows.find(
																(item) =>
																	item[previousScaleColumn] ===
																	previousScaleValue
															)?.id;
															const histid = rows.find(
																(item) =>
																	item[previousScaleColumn] ===
																	previousScaleValue
															)?.hist_id;
															console.log('id', id, histid);
															if (
																row.hist_id < histid &&
																row.hist_id >= histid - 1 &&
																row.id === id
															) {
																return (
																	<TableRow hover key={row.id}>
																		{headCells.length > 0
																			? headCells.map((cell) => {
																					return (
																						<TableCell
																							component="th"
																							id={cell.id}
																							scope="row"
																						>
																							{row[cell.id]
																								? row[cell.id]
																								: 'Null'}
																						</TableCell>
																					);
																			  })
																			: null}
																	</TableRow>
																);
															}
														}
													)}
												</TableBody>
											</Table>
										</TableContainer>
									</Paper>
								) : null}
							</Grid>
						</Grid>
						<Grid container spacing={2}>
							<Grid item>
								<Grid container direction="row">
									<Grid item>
										<Button
											color="primary"
											variant="contained"
											onClick={() => setShowNextScale(!showNextScale)}
										>
											Get Next Scale
										</Button>
									</Grid>
									<Grid item>
										<TextField
											id="standard-basic"
											label="Column Name"
											variant="outlined"
											style={{
												width: '100%',
												marginBottom: '10px',
												paddingBottom: '2px',
												marginLeft: '15px',
											}}
											value={nextScaleColumn}
											onChange={(e) => setNextScaleColumn(e.target.value)}
											// select
										/>
									</Grid>
									<Grid item>
										<TextField
											id="standard-basic"
											label="Value"
											variant="outlined"
											style={{
												width: '100%',
												marginBottom: '10px',
												paddingBottom: '2px',
												marginLeft: '15px',
											}}
											value={nextScaleValue}
											onChange={(e) => setNextScaleValue(e.target.value)}
											// select
										/>
									</Grid>
								</Grid>
								{showNextScale ? (
									<Paper className={classes.paper}>
										<TableContainer>
											<Table
												className={classes.table}
												aria-labelledby="tableTitle"
												size={dense ? 'small' : 'medium'}
												aria-label="enhanced table"
											>
												<CustomTableHead
													order={order}
													orderBy={orderBy}
													onRequestSort={handleRequestSort}
													rowCount={rows.length}
													headCells={headCells}
													checkbox={false}
												/>

												<TableBody>
													{stableSort(rows, getComparator(order, orderBy)).map(
														(row, index) => {
															const id = rows.find(
																(item) =>
																	item[nextScaleColumn] === nextScaleValue
															)?.id;
															const histid = rows.find(
																(item) =>
																	item[nextScaleColumn] === nextScaleValue
															)?.hist_id;

															if (
																row.hist_id > histid &&
																row.hist_id <= histid + 1 &&
																row.id === id
															) {
																return (
																	<TableRow hover key={row.id}>
																		{headCells.length > 0
																			? headCells.map((cell) => {
																					return (
																						<TableCell
																							component="th"
																							id={cell.id}
																							scope="row"
																						>
																							{row[cell.id]
																								? row[cell.id]
																								: 'Null'}
																						</TableCell>
																					);
																			  })
																			: null}
																	</TableRow>
																);
															}
														}
													)}
												</TableBody>
											</Table>
										</TableContainer>
									</Paper>
								) : null}
							</Grid>
						</Grid>
						<Grid container spacing={2}>
							<Grid item>
								<Button
									color="primary"
									variant="contained"
									onClick={() => showEvolutionHistory(!evolutionHistory)}
								>
									Get Evolution History
								</Button>
								{evolutionHistory ? (
									<Paper className={classes.paper}>
										<TableContainer>
											<Table
												className={classes.table}
												aria-labelledby="tableTitle"
												size={dense ? 'small' : 'medium'}
												aria-label="enhanced table"
											>
												<CustomTableHead
													order={order}
													orderBy={orderBy}
													onRequestSort={handleRequestSort}
													rowCount={rows.length}
													headCells={headCells.filter((item) => {
														return (
															(item.id === 'id' ||
																item.id === 'start_date' ||
																item.id === 'end_date') &&
															item
														);
													})}
													checkbox={false}
												/>

												<TableBody>
													{stableSort(rows, getComparator(order, orderBy)).map(
														(row, index) => {
															if (
																rows.find(
																	(item) => item.hist_id === selected[0]
																)?.id === row.id
															) {
																return (
																	<TableRow hover key={row.id}>
																		{headCells.length > 0
																			? headCells.map((cell) => {
																					console.log(cell);
																					if (
																						cell.id === 'id' ||
																						cell.id === 'start_date' ||
																						cell.id === 'end_date'
																					) {
																						return (
																							<TableCell
																								component="th"
																								id={cell.id}
																								scope="row"
																							>
																								{row[cell.id]
																									? row[cell.id]
																									: 'Null'}
																							</TableCell>
																						);
																					}
																			  })
																			: null}
																	</TableRow>
																);
															}
														}
													)}
												</TableBody>
											</Table>
										</TableContainer>
									</Paper>
								) : null}
							</Grid>
						</Grid>
						<Grid container spacing={2}>
							<Grid item>
								<Button
									color="primary"
									variant="contained"
									onClick={() => showCurrentEvolution(!CurrentEvolution)}
								>
									Get Evolution
								</Button>
								{CurrentEvolution ? (
									<Paper className={classes.paper}>
										<TableContainer>
											<Table
												className={classes.table}
												aria-labelledby="tableTitle"
												size={dense ? 'small' : 'medium'}
												aria-label="enhanced table"
											>
												<CustomTableHead
													order={order}
													orderBy={orderBy}
													onRequestSort={handleRequestSort}
													rowCount={rows.length}
													headCells={headCells.filter((item) => {
														return (
															(item.id === 'id' ||
																item.id === 'start_date' ||
																item.id === 'end_date') &&
															item
														);
													})}
													checkbox={false}
												/>

												<TableBody>
													{stableSort(rows, getComparator(order, orderBy)).map(
														(row, index) => {
															if (row.hist_id === selected[0]) {
																return (
																	<TableRow hover key={row.id}>
																		{headCells.length > 0
																			? headCells.map((cell) => {
																					console.log(cell);
																					if (
																						cell.id === 'id' ||
																						cell.id === 'start_date' ||
																						cell.id === 'end_date'
																					) {
																						return (
																							<TableCell
																								component="th"
																								id={cell.id}
																								scope="row"
																							>
																								{row[cell.id]
																									? row[cell.id]
																									: 'Null'}
																							</TableCell>
																						);
																					}
																			  })
																			: null}
																	</TableRow>
																);
															}
														}
													)}
												</TableBody>
											</Table>
										</TableContainer>
									</Paper>
								) : null}
							</Grid>
						</Grid>

						<Grid container spacing={2}>
							<Grid item>
								<Button
									color="primary"
									variant="contained"
									onClick={() => showFirstLastEvoltion(!firstLastEvoltion)}
								>
									Get First/Last Evolution
								</Button>
								{firstLastEvoltion ? (
									<Paper className={classes.paper}>
										<TableContainer>
											<Table
												className={classes.table}
												aria-labelledby="tableTitle"
												size={dense ? 'small' : 'medium'}
												aria-label="enhanced table"
											>
												<CustomTableHead
													order={order}
													orderBy={orderBy}
													onRequestSort={handleRequestSort}
													rowCount={rows.length}
													headCells={headCells.filter((item) => {
														return (
															(item.id === 'id' ||
																item.id === 'start_date' ||
																item.id === 'end_date') &&
															item
														);
													})}
													checkbox={false}
												/>

												<TableBody>
													{stableSort(rows, getComparator(order, orderBy)).map(
														(row, index) => {
															const id = rows.find(
																(item) => item.hist_id === selected[0]
															)?.id;
															const filter = rows.filter(
																(item) => item.id === id
															);
															// console.log('filter', filter);
															// const temp =
															// 	row.hist_id === selected[0] ? row : null;
															// let newrows = rows.filter((item) => {
															// 	console.log(item, temp);

															// 	if (item.id === temp?.id) return item;
															// });
															// console.log(temp);
															if (
																row.hist_id === filter[0].hist_id ||
																row.hist_id ===
																	filter[filter.length - 1].hist_id
															) {
																return (
																	<TableRow hover key={row.id}>
																		{headCells.length > 0
																			? headCells.map((cell) => {
																					console.log(cell);
																					if (
																						cell.id === 'id' ||
																						cell.id === 'start_date' ||
																						cell.id === 'end_date'
																					) {
																						return (
																							<TableCell
																								component="th"
																								id={cell.id}
																								scope="row"
																							>
																								{row[cell.id]
																									? row[cell.id]
																									: 'Null'}
																							</TableCell>
																						);
																					}
																			  })
																			: null}
																	</TableRow>
																);
															}
														}
													)}
												</TableBody>
											</Table>
										</TableContainer>
									</Paper>
								) : null}
							</Grid>
						</Grid>
						<Grid container spacing={2}>
							<Grid item>
								<Button
									color="primary"
									variant="contained"
									onClick={() => setEvolutionVal(!evolutionVal)}
								>
									Get Evolution 'Val1'='Val2'
								</Button>
								{evolutionVal ? (
									<Paper className={classes.paper}>
										<TableContainer>
											<Table
												className={classes.table}
												aria-labelledby="tableTitle"
												size={dense ? 'small' : 'medium'}
												aria-label="enhanced table"
											>
												<CustomTableHead
													order={order}
													orderBy={orderBy}
													onRequestSort={handleRequestSort}
													rowCount={rows.length}
													headCells={headCells}
													checkbox={false}
												/>

												<TableBody>
													{stableSort(rows, getComparator(order, orderBy)).map(
														(row, index) => {
															const id = rows.find(
																(item) => item.hist_id === selected[0]
															)?.id;
															const nextid = rows.find(
																(item) => item.hist_id === selected[1]
															)?.hist_id;
															const histid = rows.find(
																(item) => item.hist_id === selected[1]
															)?.hist_id;

															if (
																row.hist_id >= nextid ||
																(row.hist_id <= histid && row.id === id)
															) {
																return (
																	<TableRow hover key={row.id}>
																		{headCells.length > 0
																			? headCells.map((cell) => {
																					console.log(cell);

																					return (
																						<TableCell
																							component="th"
																							id={cell.id}
																							scope="row"
																						>
																							{row[cell.id]
																								? row[cell.id]
																								: 'Null'}
																						</TableCell>
																					);
																			  })
																			: null}
																	</TableRow>
																);
															}
														}
													)}
												</TableBody>
											</Table>
										</TableContainer>
									</Paper>
								) : null}
							</Grid>
						</Grid>
						<Grid container spacing={2}>
							<Grid item>
								<Button
									color="primary"
									variant="contained"
									onClick={() => setColumnTimestamp(!columnTimestamp)}
								>
									Get Column Timestamps 'Val'
								</Button>
								{columnTimestamp ? (
									<Paper className={classes.paper}>
										<TableContainer>
											<Table
												className={classes.table}
												aria-labelledby="tableTitle"
												size={dense ? 'small' : 'medium'}
												aria-label="enhanced table"
											>
												<CustomTableHead
													order={order}
													orderBy={orderBy}
													onRequestSort={handleRequestSort}
													rowCount={rows.length}
													headCells={headCells.filter((item) => {
														return (
															(item.id === 'id' ||
																item.id === 'start_date' ||
																item.id === 'end_date') &&
															item
														);
													})}
													checkbox={false}
												/>

												<TableBody>
													{stableSort(rows, getComparator(order, orderBy)).map(
														(row, index) => {
															const id = rows.find(
																(item) => item.hist_id === selected[0]
															)?.id;

															if (row.hist_id === selected[0]) {
																return (
																	<TableRow hover key={row.id}>
																		{headCells.length > 0
																			? headCells.map((cell) => {
																					console.log(cell);
																					if (
																						cell.id === 'id' ||
																						cell.id === 'start_date' ||
																						cell.id === 'end_date'
																					) {
																						return (
																							<TableCell
																								component="th"
																								id={cell.id}
																								scope="row"
																							>
																								{row[cell.id]
																									? row[cell.id]
																									: 'Null'}
																							</TableCell>
																						);
																					}
																			  })
																			: null}
																	</TableRow>
																);
															}
														}
													)}
												</TableBody>
											</Table>
										</TableContainer>
									</Paper>
								) : null}
							</Grid>
						</Grid>
						<Grid container spacing={2}>
							<Grid item>
								<Grid container spacing={2}>
									<Grid item>
										<Button
											color="primary"
											variant="contained"
											onClick={() => setShowColumnValue(!showColumnValue)}
										>
											Get Column Value
										</Button>
									</Grid>
									<Grid item>
										<TextField
											id="standard-basic"
											variant="outlined"
											// value={addRecordFields[item.label]}
											type={'date'}
											style={{ width: '100%', marginBottom: '10px' }}
											onChange={(e) => setSelectedDate(e.target.value)}
										></TextField>
									</Grid>
								</Grid>
							</Grid>
							{showColumnValue ? (
								<Paper className={classes.paper}>
									<TableContainer>
										<Table
											className={classes.table}
											aria-labelledby="tableTitle"
											size={dense ? 'small' : 'medium'}
											aria-label="enhanced table"
										>
											<CustomTableHead
												order={order}
												orderBy={orderBy}
												onRequestSort={handleRequestSort}
												rowCount={rows.length}
												headCells={headCells}
												checkbox={false}
											/>

											<TableBody>
												{stableSort(rows, getComparator(order, orderBy)).map(
													(row, index) => {
														const id = rows.find(
															(item) =>
																moment(item.start_date).format('LL') ===
																moment(selectedDate).format('LL')
														)?.id;

														if (row.id === id) {
															return (
																<TableRow hover key={row.id}>
																	{headCells.length > 0
																		? headCells.map((cell) => {
																				return (
																					<TableCell
																						component="th"
																						id={cell.id}
																						scope="row"
																					>
																						{row[cell.id]
																							? row[cell.id]
																							: 'Null'}
																					</TableCell>
																				);
																		  })
																		: null}
																</TableRow>
															);
														}
													}
												)}
											</TableBody>
										</Table>
									</TableContainer>
								</Paper>
							) : null}
						</Grid>
						<Grid container spacing={2}>
							<Grid item>
								<Button
									color="primary"
									variant="contained"
									onClick={() => setShowColumnDate(!showColumnDate)}
								>
									Get Column Timestamp
								</Button>
								{showColumnDate ? (
									<Paper className={classes.paper}>
										<TableContainer>
											<Table
												className={classes.table}
												aria-labelledby="tableTitle"
												size={dense ? 'small' : 'medium'}
												aria-label="enhanced table"
											>
												<CustomTableHead
													order={order}
													orderBy={orderBy}
													onRequestSort={handleRequestSort}
													rowCount={rows.length}
													headCells={headCells.filter((item) => {
														return (
															(item.id === 'id' ||
																item.id === 'start_date' ||
																item.id === 'end_date') &&
															item
														);
													})}
													checkbox={false}
												/>

												<TableBody>
													{stableSort(rows, getComparator(order, orderBy)).map(
														(row, index) => {
															if (row.hist_id === selected[0]) {
																return (
																	<TableRow hover key={row.id}>
																		{headCells.length > 0
																			? headCells.map((cell) => {
																					console.log(cell);
																					if (
																						cell.id === 'id' ||
																						cell.id === 'start_date' ||
																						cell.id === 'end_date'
																					) {
																						return (
																							<TableCell
																								component="th"
																								id={cell.id}
																								scope="row"
																							>
																								{row[cell.id]
																									? row[cell.id]
																									: 'Null'}
																							</TableCell>
																						);
																					}
																			  })
																			: null}
																	</TableRow>
																);
															}
														}
													)}
												</TableBody>
											</Table>
										</TableContainer>
									</Paper>
								) : null}
							</Grid>
						</Grid>
					</Grid>
				</>
			) : null}
			<Dialog
				open={addModal === 'add' || addModal === 'edit'}
				onClose={handleClose}
				aria-labelledby="alert-dialog-title"
				aria-describedby="alert-dialog-description"
			>
				<DialogTitle id="alert-dialog-title">
					{addModal === 'add' ? 'Create New Record' : 'Edit Record'}
				</DialogTitle>
				<DialogContent>
					<form className={classes.root} noValidate autoComplete="off">
						{headCells.length && headCells.length > 0
							? headCells.map((item) => {
									let type = item.type.search('enum') !== -1 ? 'select' : '';
									let st1;
									if (type === 'select') {
										st1 = item.type.slice(5);
										st1 = st1.replace(')', '');

										st1 = st1.split(',');
									}

									if (item.id === 'id') {
										return null;
									} else if (type === 'select') {
										return (
											<TextField
												id="standard-basic"
												label={item.label}
												variant="outlined"
												style={{ width: '100%', marginBottom: '10px' }}
												value={addRecordFields[item.label]}
												onChange={(e) =>
													setAddRecordFields({
														...addRecordFields,
														[item.label]: e.target.value,
													})
												}
												select
											>
												{type === 'select'
													? st1.map((option) => {
															return (
																<MenuItem
																	key={option}
																	value={option.replaceAll(`'`, '')}
																>
																	{option.replaceAll(`'`, '')}
																</MenuItem>
															);
													  })
													: null}
											</TextField>
										);
									} else {
										return (
											<TextField
												id="standard-basic"
												label={item.type === 'datetime' ? '' : item.label}
												variant="outlined"
												value={addRecordFields[item.label]}
												type={
													item.type === 'datetime'
														? 'date'
														: item.type === 'int' ||
														  item.type === 'tinyint' ||
														  item.type === 'smallint' ||
														  item.type === 'mediumint' ||
														  item.type === 'bigint'
														? 'number'
														: 'text'
												}
												style={{ width: '100%', marginBottom: '10px' }}
												onChange={(e) =>
													setAddRecordFields({
														...addRecordFields,
														[item.label]: e.target.value,
													})
												}
											></TextField>
										);
									}
							  })
							: null}
					</form>
				</DialogContent>
				<DialogActions>
					<Button
						onClick={addModal === 'add' ? handleCreateRecord : handleEditRecord}
						color="primary"
						variant="contained"
					>
						Submit
					</Button>
					<Button onClick={handleClose} color="primary" autoFocus>
						Cancel
					</Button>
				</DialogActions>
			</Dialog>
			<Dialog
				open={deleteModal}
				onClose={handleClose}
				aria-labelledby="alert-dialog-title"
				aria-describedby="alert-dialog-description"
			>
				<DialogTitle id="alert-dialog-title">Delete Record</DialogTitle>
				<DialogContent>Are you sure to delete this record?</DialogContent>
				<DialogActions>
					<Button
						onClick={handleDeleteRecord}
						color="primary"
						variant="contained"
					>
						Delete
					</Button>
					<Button onClick={handleClose} color="primary" autoFocus>
						Cancel
					</Button>
				</DialogActions>
			</Dialog>
		</div>
	);
};
export default CustomDataTable;
