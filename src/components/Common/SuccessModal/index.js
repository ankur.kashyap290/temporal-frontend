import React from "react";
import { Typography, Button, Grid } from "@material-ui/core/";
import useStyles from "../../../styles/successModal";
import DoneAllIcon from '@material-ui/icons/DoneAll';


const SuccessModal = props => {
  const classes = useStyles();
  const { title, description, buttonText, handlerFunction } = props;

  return (
    <React.Fragment>
      <div className={classes.hiddenSm}>
        <div className={classes.successContainer}>
         <DoneAllIcon style={{ fontSize: "120px", margin: "25px" , }} color="secondary" /> 
          <Typography variant="h5" className={classes.successModalHeading}>
            {title}
          </Typography>
          <Typography variant="body1" className={classes.successModalConfirm}>
            {description}
          </Typography>
        </div>
        <Grid container justify="center">
          <Button
            variant="outlined"
            color="secondary"
            size="large"
            style={{ width: "160px", margin: "20px 0px" }}
            disableRipple
            onClick={() => handlerFunction()}
          >
            {buttonText}
          </Button>
        </Grid>
      </div>
      <Grid container direction="column" justify="space-around" className={classes.hiddenXs}>
        <Grid item>
          <div className={classes.successContainer}>
           <DoneAllIcon style={{ fontSize: "120px", margin: "25px" }} color="secondary"/> 
            <Typography variant="h5" className={classes.successModalHeading}>
              {title}
            </Typography>
            <Typography variant="body1" className={classes.successModalConfirm}>
              {description}
            </Typography>
          </div>
        </Grid>
        <Grid item style={{ textAlign: "center" }}>
          <Button
            variant="outlined"
            color="secondary"
            size="large"
            style={{ width: "160px", margin: "20px 0px" }}
            disableRipple
            onClick={() => handlerFunction()}
          >
            {buttonText}
          </Button>
        </Grid>
      </Grid>
    </React.Fragment>
  );
};

export default SuccessModal;
