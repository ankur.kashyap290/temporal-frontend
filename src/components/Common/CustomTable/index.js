import React, { useState } from 'react';
import moment from 'moment';
import {
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TablePagination,
	TableRow,
	Menu,
	withStyles,
	MenuItem,
	Button,
	ListItemIcon,
	ListItemText,
	Typography,
	Grid,
} from '@material-ui/core';
import { Scrollbars } from 'react-custom-scrollbars';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import TableToolbar from './tableToolbar';
import CustomTableHead from './tableHead';
import { useStyles } from '../../../styles/customTable';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

function descendingComparator(a, b, orderBy) {
	if (b[orderBy] < a[orderBy]) {
		return -1;
	}
	if (b[orderBy] > a[orderBy]) {
		return 1;
	}
	return 0;
}

function getComparator(order, orderBy) {
	return order === 'desc'
		? (a, b) => descendingComparator(a, b, orderBy)
		: (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
	const stabilizedThis = array.map((el, index) => [el, index]);
	stabilizedThis.sort((a, b) => {
		const order = comparator(a[0], b[0]);
		if (order !== 0) return order;
		return a[1] - b[1];
	});
	return stabilizedThis.map((el) => el[0]);
}

const StyledMenu = withStyles({
	paper: {
		border: '1px solid #d3d4d5',
	},
})((props) => (
	<Menu
		elevation={0}
		getContentAnchorEl={null}
		anchorOrigin={{
			vertical: 'bottom',
			horizontal: 'center',
		}}
		transformOrigin={{
			vertical: 'top',
			horizontal: 'center',
		}}
		{...props}
	/>
));

const StyledMenuItem = withStyles((theme) => ({
	root: {
		'&:focus': {
			backgroundColor: theme.palette.primary.main,
			'& .MuiListItemIcon-root, & .MuiListItemText-primary': {
				color: theme.custom.black100,
			},
		},
	},
}))(MenuItem);

const CustomTable = ({ history, rows, headCells, pageType, data, token }) => {
	const classes = useStyles();
	const [order, setOrder] = useState('asc');
	const [orderBy, setOrderBy] = useState('date');
	const [selected, setSelected] = useState([]);
	const [page, setPage] = useState(0);
	const [dense, setDense] = useState(false);
	const [rowsPerPage, setRowsPerPage] = useState(5);

	const [anchorEl, setAnchorEl] = useState(null);

	const handleClick = (event) => {
		event.preventDefault();
		event.stopPropagation();
		setAnchorEl(event.currentTarget);
	};

	const handleClose = () => {
		setAnchorEl(null);
	};

	const handleRequestSort = (event, property) => {
		const isAsc = orderBy === property && order === 'asc';
		setOrder(isAsc ? 'desc' : 'asc');
		setOrderBy(property);
	};

	const handleSelectAllClick = (event) => {
		if (event.target.checked) {
			const newSelecteds = rows.map((n) => n.id);
			setSelected(newSelecteds);
			return;
		}
		setSelected([]);
	};

	const handleClickCheckbox = (e, id) => {
		e.preventDefault();
		e.stopPropagation();
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(
				selected.slice(0, selectedIndex),
				selected.slice(selectedIndex + 1)
			);
		}

		setSelected(newSelected);
	};

	const handleViewDatabase = (dbname, tablename) => {
		console.log(dbname, tablename);
		if (pageType === 'db') {
			history.push(`/database/${dbname}`);
		} else if (pageType === 'tables') {
			history.push(`/database/${dbname}/${tablename}`);
		} else {
			console.log('*****', `/tabledata/${dbname}/${tablename}`);
			history.push(`/tabledata/${dbname}/${tablename}`);
		}
	};

	const handleChangePage = (event, newPage) => {
		setPage(newPage);
	};

	const handleChangeRowsPerPage = (event) => {
		setRowsPerPage(parseInt(event.target.value, 10));
		setPage(0);
	};

	const handleChangeDense = (event) => {
		setDense(event.target.checked);
	};

	const handleAction = (e, id, action) => {
		e.preventDefault();
		e.stopPropagation();
		handleClose();
	};

	const isSelected = (id) => selected.indexOf(id) !== -1;

	const emptyRows =
		rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
	return (
		<div className={classes.root}>
			<Paper className={classes.paper}>
				{/* <TableToolbar numSelected={selected.length} /> */}
				<TableContainer>
					<Table
						className={classes.table}
						aria-labelledby="tableTitle"
						size={dense ? 'small' : 'medium'}
						aria-label="enhanced table"
					>
						<CustomTableHead
							numSelected={selected.length}
							order={order}
							orderBy={orderBy}
							onSelectAllClick={handleSelectAllClick}
							onRequestSort={handleRequestSort}
							rowCount={rows.length}
							headCells={headCells}
						/>

						<TableBody>
							{rows.length === 0 ? (
								<Typography
									variant="body1"
									style={{ padding: '20px', width: '200px' }}
								>
									No records found
								</Typography>
							) : null}
							{stableSort(rows, getComparator(order, orderBy))
								.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
								.map((row, index) => {
									const isItemSelected = isSelected(row.id);
									const labelId = `enhanced-table-checkbox-${index}`;

									return (
										<TableRow
											hover
											onClick={() =>
												handleViewDatabase(
													pageType === 'db'
														? row.database
														: pageType === 'tables'
														? data[index].TABLE_SCHEMA
														: data[index].TABLE_SCHEMA,
													pageType === 'db'
														? row.database
														: pageType === 'tables'
														? row.tableName
														: data[index].TABLE_NAME
												)
											}
											// role="checkbox"
											aria-checked={isItemSelected}
											tabIndex={-1}
											key={index}
											selected={isItemSelected}
										>
											{/* <TableCell padding="checkbox">
												<Checkbox
													checked={isItemSelected}
													inputProps={{ 'aria-labelledby': labelId }}
													onClick={(e) => handleClickCheckbox(e, row.id)}
												/>
											</TableCell> */}
											<TableCell
												component="th"
												id={labelId}
												scope="row"
												// padding="none"
											>
												{row.index}
											</TableCell>
											<TableCell>
												{pageType === 'db'
													? row.database
													: pageType === 'tables'
													? row.tableName
													: row.columnName}
											</TableCell>
											<TableCell>
												{pageType === 'db'
													? null
													: pageType === 'tables'
													? row.createdOn
													: row.columnType}
											</TableCell>
											<TableCell>
												{pageType === 'order' ? row.contact : row.customer}
											</TableCell>
											<TableCell>
												{pageType === 'order' ? (
													<div>
														<Button
															aria-controls="customized-menu"
															aria-haspopup="true"
															variant="text"
															onClick={handleClick}
															endIcon={
																anchorEl ? (
																	<KeyboardArrowUpIcon />
																) : (
																	<KeyboardArrowDownIcon />
																)
															}
														>
															{row.status}
														</Button>
														<StyledMenu
															id="customized-menu"
															anchorEl={anchorEl}
															keepMounted
															open={Boolean(anchorEl)}
															onClose={handleClose}
														>
															<StyledMenuItem
																onClick={(e) =>
																	handleAction(e, row.id, 'processing')
																}
															>
																<ListItemIcon>
																	<SendIcon fontSize="small" />
																</ListItemIcon>
																<ListItemText primary="Order processing in progress" />
															</StyledMenuItem>
															<StyledMenuItem
																onClick={(e) =>
																	handleAction(e, row.id, 'waiting')
																}
															>
																<ListItemIcon>
																	<DraftsIcon fontSize="small" />
																</ListItemIcon>
																<ListItemText primary="Waiting for construction" />
															</StyledMenuItem>
															<StyledMenuItem
																onClick={(e) =>
																	handleAction(e, row.id, 'cancel')
																}
															>
																<ListItemIcon>
																	<InboxIcon fontSize="small" />
																</ListItemIcon>
																<ListItemText primary="Cancel" />
															</StyledMenuItem>
															<StyledMenuItem
																onClick={(e) =>
																	handleAction(e, row.id, 'complete')
																}
															>
																<ListItemIcon>
																	<InboxIcon fontSize="small" />
																</ListItemIcon>
																<ListItemText primary="Completed" />
															</StyledMenuItem>
														</StyledMenu>
													</div>
												) : (
													row.message
												)}
											</TableCell>
										</TableRow>
									);
								})}
							{emptyRows > 0 && (
								<TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
									<TableCell colSpan={6} />
								</TableRow>
							)}
						</TableBody>
					</Table>
				</TableContainer>
				<TablePagination
					rowsPerPageOptions={[5, 10, 25]}
					component="div"
					count={rows.length}
					rowsPerPage={rowsPerPage}
					page={page}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</Paper>

			<FormControlLabel
				control={<Switch checked={dense} onChange={handleChangeDense} />}
				label="Dense padding"
			/>
		</div>
	);
};
export default CustomTable;
