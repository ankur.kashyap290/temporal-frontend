import React from 'react';
import { Backdrop, CircularProgress } from  '@material-ui/core';
// import styles from './index.module.less';

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: 1201,
    color: '#fff',
  },
}));

const OverlayLoader =()=> {
    const { loading, fullWidth } = this.props;
    const classes = useStyles();

    return loading ? (
      // <div className={fullWidth ? styles.FullWidthLoading : styles.loading}>
      <Backdrop className={classes.backdrop} open={loading} >
        {/* onClick={handleClose} */}
        <CircularProgress />
      </Backdrop>
      // </div>
    ) : (
      ''
    );
}

export default OverlayLoader;
