import React from 'react';
import {
	AppBar,
	Toolbar,
	Typography,
	Link,
	Grid,
	Drawer,
	Divider,
	List,
	ListItem,
	ListItemText,
} from '@material-ui/core';
import useStyles from '../../styles/appHeader';
import { sideNav } from '../../configs/app.config';

const AppHeader = ({ history }) => {
	const classes = useStyles();
	const handleNavClick = (item) => {
		history.push(`/${item.url}`);
	};
	return (
		<React.Fragment>
			<AppBar position="fixed" className={classes.appBar}>
				<Toolbar>
					<Grid container direction="row" justify="space-between">
						<Grid item>
							<Typography className={classes.logo} variant="h6" noWrap>
								Temporal Database
							</Typography>
						</Grid>
						<Grid item>
							{/* <div className={classes.sectionDesktop}>
								<Link href="#" onClick={console.log('shop')} color="inherit">
									Shop
								</Link>{' '}
								<Link
									href="#"
									onClick={console.log('construction')}
									color="inherit"
								>
									Construction
								</Link>
							</div> */}
						</Grid>
					</Grid>
				</Toolbar>
			</AppBar>
			<Drawer
				className={classes.drawer}
				variant="permanent"
				classes={{
					paper: classes.drawerPaper,
				}}
			>
				<Toolbar />
				<div className={classes.drawerContainer}>
					<List>
						{sideNav.map((item, index) => (
							<ListItem
								button
								key={`Nav_${index}`}
								onClick={() => handleNavClick(item)}
							>
								{/* <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon> */}
								<ListItemText primary={item.label} />
							</ListItem>
						))}
					</List>
					<Divider />
				</div>
			</Drawer>
		</React.Fragment>
	);
};

export default AppHeader;
