import React, { useState } from 'react';
import {
	TextField,
	Checkbox,
	FormControlLabel,
	Typography,
	FormControl,
	FormGroup,
	Button,
	Grid,
	InputAdornment,
	IconButton,
	Link,
} from '@material-ui/core';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import useStyles from '../../styles/login';
import Visibility from '@material-ui/icons/Visibility';
import { userLogin } from '../../redux/user/actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Alert from '@material-ui/lab/Alert';

const LoginForm = ({ userLogin, error, history }) => {
	const [agree, setAgree] = useState(false);
	const [passwordVisibility, showPassword] = useState(false);
	const classes = useStyles();

	const handleCheckbox = () => {
		setAgree(!agree);
	};

	const handleClickShowPassword = () => {
		showPassword(!passwordVisibility);
	};

	const validationSchema = () => {
		return Yup.object().shape({
			email: Yup.string()
				.required('* Email is required')
				.email('* Enter a valid email'),
			password: Yup.string().required('* Password is required'),
		});
	};

	const validate = (getValidationSchema) => {
		return (values) => {
			const validationSchema = getValidationSchema(values);
			try {
				validationSchema.validateSync(values, { abortEarly: false });
				return {};
			} catch (error) {
				return getErrorsFromValidationError(error);
			}
		};
	};

	const getErrorsFromValidationError = (validationError) => {
		const FIRST_ERROR = 0;
		return validationError.inner.reduce((errors, error) => {
			return {
				...errors,
				[error.path]: error.errors[FIRST_ERROR],
			};
		}, {});
	};

	const initialValues = {
		email: '',
		password: '',
	};

	const handleSubmit = (values) => {
		userLogin(
			{
				email: values.email,
				password: values.password,
				history,
			},
			agree
		);
	};

	return (
		<React.Fragment>
			<Formik
				initialValues={initialValues}
				validate={validate(validationSchema)}
				onSubmit={handleSubmit}
				render={({ errors, handleChange, handleSubmit }) => (
					<Form onSubmit={handleSubmit}>
						<FormControl style={{ width: '100%' }}>
							<FormGroup aria-label="position">
								<Typography
									variant="h4"
									className={classes.signUpAndSignInHeading}
								>
									Login to Temporal Database App
								</Typography>
								{error && (
									<div style={{ margin: '10px 0px 20px 0px' }}>
										<Alert severity="error">{error}</Alert>
									</div>
								)}
								<TextField
									variant="outlined"
									label="Email"
									id="email"
									className={classes.inputFields}
									placeholder="EMAIL"
									onChange={handleChange}
									helperText={
										errors.email && (
											<span className={classes.errorText}>{errors.email}</span>
										)
									}
									InputProps={{
										classes: {
											root: classes.inputBorder,
										},
									}}
								/>
								<TextField
									variant="outlined"
									label="Password"
									id="password"
									className={classes.inputFields}
									placeholder="PASSWORD"
									onChange={handleChange}
									helperText={
										errors.password && (
											<span className={classes.errorText}>
												{errors.password}
											</span>
										)
									}
									type={passwordVisibility ? 'text' : 'password'}
									InputProps={{
										classes: {
											root: classes.inputBorder,
										},
										endAdornment: (
											<InputAdornment position="end">
												<IconButton
													aria-label="toggle password visibility"
													onClick={handleClickShowPassword}
													edge="end"
												>
													<Visibility
														className={
															passwordVisibility
																? classes.showPassword
																: classes.hidePassword
														}
													/>
												</IconButton>
											</InputAdornment>
										),
									}}
								/>

								<FormControlLabel
									value="start"
									control={
										<Checkbox
											checked={agree}
											onChange={handleCheckbox}
											color="secondary"
										/>
									}
									label={
										<div>
											<Typography
												variant="body2"
												className={classes.rememberMe}
											>
												Remember me
											</Typography>
										</div>
									}
									labelPlacement="end"
									className={classes.checkbox}
								/>
							</FormGroup>
							<Grid container justify="center">
								<FormGroup>
									<FormControl>
										<Button
											variant="contained"
											color="secondary"
											size="large"
											style={{ width: '200px', margin: '20px 0px' }}
											disableRipple
											type="submit"
										>
											Login
										</Button>
									</FormControl>
								</FormGroup>
							</Grid>
							<Grid container justify="center">
								<Link href="/register" className={classes.forgotLink}>
									Not a member? Register here!
								</Link>
							</Grid>
						</FormControl>
					</Form>
				)}
			/>
		</React.Fragment>
	);
};

const mapStateToProps = ({ user }) => {
	return {
		profile: user.user,
		token: user.token,
		loading: user.loading,
		error: user.error,
	};
};

const mapDispatchToProps = (dispatch) =>
	bindActionCreators(
		{
			userLogin,
		},
		dispatch
	);

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
