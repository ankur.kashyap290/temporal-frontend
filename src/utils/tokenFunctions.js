import { tokenSalt, userKey } from '../configs/app.config';

export function getToken() {
	return localStorage.getItem(tokenSalt) || null;
}

export function setToken(token) {
	return localStorage.setItem(tokenSalt, token || null);
}

export function getProfile() {
	const userProfile = localStorage.getItem(userKey);
	return userProfile ? JSON.parse(userProfile) : null;
}

export function setProfile(user) {
	return localStorage.setItem(userKey, JSON.stringify(user) || null);
}
