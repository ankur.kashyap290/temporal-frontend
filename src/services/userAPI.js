import request from '../utils/request';
import { API_URL } from '../configs/app.config';

export const userAPI = {
	async userRegister(payload) {
		const response = await request(`${API_URL}/api/users`, {
			method: 'POST',
			body: {
				...payload,
			},
		});
		const result = {};
		if (response.data.status.toLowerCase() !== 'ok') {
			result.status = 'Error';
			result.msg = response.data.msg;
		} else {
			result.status = 'OK';
			result.data = response.data.data;
		}
		return result;
	},

	async userLogin(payload) {
		const response = await request(`${API_URL}/api/users/sign-in`, {
			method: 'POST',
			body: {
				...payload,
			},
		});
		const result = {};
		if (response.data.status.toLowerCase() !== 'ok') {
			result.status = 'Error';
			result.msg = response.data.msg;
		} else {
			result.status = 'OK';
			result.token = response.data.data;
		}
		return result;
	},
};
