import request from '../utils/request';
import { API_URL } from '../configs/app.config';

export const databaseAPI = {
	async getAllDatabases(token) {
		const response = await request(`${API_URL}/api/databases`, {
			method: 'GET',
			// headers: { Authorization: `Bearer ${token}` },
		});
		const result = {};
		if (response.data.status.toLowerCase() !== 'ok') {
			result.status = 'Error';
			result.msg = response.data.msg;
		} else {
			result.status = 'OK';
			result.data = response.data;
		}
		return result;
	},

	async getDatabase(payload, token) {
		const response = await request(
			`${API_URL}/api/databases/database/${payload}`,
			{
				method: 'GET',
				// headers: { Authorization: `Bearer ${token}` },
			}
		);

		const result = {};
		if (response.data.status.toLowerCase() !== 'ok') {
			result.status = 'Error';
			result.msg = response.data.msg;
		} else {
			result.status = 'OK';
			result.data = response.data;
		}
		return result;
	},

	async getTable(payload, token) {
		const response = await request(
			`${API_URL}/api/databases/table/${payload.databaseName}/${payload.tableName}`,
			{
				method: 'GET',
				// headers: { Authorization: `Bearer ${token}` },
			}
		);
		const result = {};
		if (response.data.status.toLowerCase() !== 'ok') {
			result.status = 'Error';
			result.msg = response.data.msg;
		} else {
			result.status = 'OK';
			result.data = response.data;
		}
		return result;
	},

	async getTableData(payload, token) {
		const response = await request(
			`${API_URL}/api/databases/tabledata/${payload.databaseName}/${payload.tableName}`,
			{
				method: 'GET',
				// headers: { Authorization: `Bearer ${token}` },
			}
		);
		const result = {};
		if (response.data.status.toLowerCase() !== 'ok') {
			result.status = 'Error';
			result.msg = response.data.msg;
		} else {
			result.status = 'OK';
			result.data = response.data;
		}
		return result;
	},

	async createRecord(payload, token) {
		console.log('response', payload);
		const response = await request(`${API_URL}/api/databases/create-record`, {
			method: 'POST',
			body: { ...payload },
			// headers: { Authorization: `Bearer ${token}` },
		});
		console.log('response', payload);
		const result = {};
		if (response.data.status.toLowerCase() !== 'ok') {
			result.status = 'Error';
			result.msg = response.data.msg;
		} else {
			result.status = 'OK';
			result.data = response.data;
		}
		return result;
	},

	async updateRecord(payload, token) {
		const response = await request(`${API_URL}/api/databases/update-record`, {
			method: 'POST',
			body: { ...payload },
			// headers: { Authorization: `Bearer ${token}` },
		});
		const result = {};
		if (response.data.status.toLowerCase() !== 'ok') {
			result.status = 'Error';
			result.msg = response.data.msg;
		} else {
			result.status = 'OK';
			result.data = response.data;
		}
		return result;
	},

	async deleteRecord(payload, token) {
		const response = await request(`${API_URL}/api/databases/delete-record`, {
			method: 'POST',
			body: { ...payload },
			// headers: { Authorization: `Bearer ${token}` },
		});
		const result = {};
		if (response.data.status.toLowerCase() !== 'ok') {
			result.status = 'Error';
			result.msg = response.data.msg;
		} else {
			result.status = 'OK';
			result.data = response.data;
		}
		return result;
	},

	async runOperators(payload, token) {
		const response = await request(`${API_URL}/api/databases/run-operators`, {
			method: 'POST',
			body: { ...payload },
			// headers: { Authorization: `Bearer ${token}` },
		});
		const result = {};
		if (response.data.status.toLowerCase() !== 'ok') {
			result.status = 'Error';
			result.msg = response.data.msg;
		} else {
			result.status = 'OK';
			result.data = response.data;
		}
		return result;
	},
};
