import userReducer from './user/reducer';
import databaseReducer from './database/reducer';

export { userReducer as user, databaseReducer as database };
