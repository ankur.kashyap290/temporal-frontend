import {
	USER_REGISTER,
	USER_REGISTER_RECEIVE,
	USER_REGISTER_ERROR,
	USER_LOGIN,
	USER_LOGIN_RECEIVE,
	USER_LOGIN_ERROR,
	SET_TOKEN,
	SET_PROFILE,
	USER_LOGOUT,
	USER_LOGOUT_ERROR,
	USER_LOGOUT_RECEIVE,
} from '../actionTypes';
import _ from 'lodash';

const initState = {
	loading: false,
	profile: null,
	token: '',
	error: null,
};

const userReducer = (state = initState, action = {}) => {
	switch (action.type) {
		case USER_REGISTER: {
			return {
				...state,
				loading: true,
				error: null,
			};
		}
		case USER_REGISTER_RECEIVE: {
			return {
				...state,
				loading: false,
			};
		}
		case USER_REGISTER_ERROR: {
			return {
				...state,
				loading: false,
				error: action.error,
			};
		}
		case USER_LOGIN: {
			return {
				...state,
				loading: true,
				error: null,
			};
		}
		case USER_LOGIN_RECEIVE: {
			return {
				...state,
				loading: false,
				token: action.payload.token,
				profile: action.payload.profile,
			};
		}
		case USER_LOGIN_ERROR: {
			return {
				...state,
				loading: false,
				error: action.error,
			};
		}

		case SET_TOKEN: {
			return {
				...state,
				token: action.token,
			};
		}
		case SET_PROFILE: {
			return {
				...state,
				profile: action.profile,
			};
		}
		case USER_LOGOUT: {
			return {
				...state,
				loading: true,
			};
		}
		case USER_LOGOUT_RECEIVE: {
			return {
				...initState,
			};
		}
		case USER_LOGOUT_ERROR: {
			return {
				...state,
				loading: false,
			};
		}

		default:
			return state;
	}
};

export default userReducer;
