import { takeEvery, put, call } from 'redux-saga/effects';
import { USER_REGISTER, USER_LOGIN, USER_LOGOUT } from '../actionTypes';
import {
	userRegisterReceive,
	userRegisterError,
	userLoginReceive,
	userLoginError,
	userLogoutReceive,
	userLogoutError,
} from '../user/actions';
import { userAPI } from '../../services/userAPI';
import { setProfile, setToken } from '../../utils/tokenFunctions';

export function* userRegister() {
	yield takeEvery(USER_REGISTER, function* ({ payload }) {
		const { history, ...params } = payload;
		try {
			const response = yield call(userAPI.userRegister, params);
			if (response.status.toLowerCase() === 'ok') {
				yield put(userRegisterReceive(response.data.merchant));
				history.push('/login');
			} else {
				yield put(userRegisterError(response.msg));
			}
		} catch (ex) {
			yield put(userRegisterError('Error while sign up'));
		}
	});
}

export function* userLogin() {
	yield takeEvery(USER_LOGIN, function* ({ payload }) {
		const { history, ...params } = payload;
		try {
			const response = yield call(userAPI.userLogin, params);

			if (response.status.toLowerCase() === 'ok') {
				yield put(
					userLoginReceive({
						token: response.token.token,
						profile: response.token.validUser,
					})
				);
				setToken(response.token.token);
				setProfile(response.token.validUser);
				history.push('/');
			} else {
				yield put(userLoginError(response.msg));
			}
		} catch (ex) {
			yield put(userLoginError('Error while sign in'));
		}
	});
}

export function* userLogout() {
	yield takeEvery(USER_LOGOUT, function* () {
		try {
			yield put(userLogoutReceive());
			setToken();
			setProfile();
		} catch (ex) {
			console.log('---Error while logout', ex);
		}
	});
}
