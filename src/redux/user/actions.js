import {
	USER_REGISTER,
	USER_REGISTER_RECEIVE,
	USER_REGISTER_ERROR,
	USER_LOGIN,
	USER_LOGIN_RECEIVE,
	USER_LOGIN_ERROR,
	SET_TOKEN,
	SET_PROFILE,
	USER_LOGOUT,
	USER_LOGOUT_ERROR,
	USER_LOGOUT_RECEIVE,
} from '../actionTypes';

export const userRegister = (payload, history) => {
	return { type: USER_REGISTER, payload, history };
};

export const userRegisterReceive = (payload) => {
	return { type: USER_REGISTER_RECEIVE, payload };
};

export const userRegisterError = (error) => {
	return { type: USER_REGISTER_ERROR, error };
};

export const userLogin = (payload, history) => {
	return { type: USER_LOGIN, payload, history };
};

export const userLoginReceive = (payload) => {
	return { type: USER_LOGIN_RECEIVE, payload };
};

export const userLoginError = (error) => {
	return { type: USER_LOGIN_ERROR, error };
};

export const setToken = (token) => {
	return { type: SET_TOKEN, token };
};

export const setProfile = (profile) => {
	return { type: SET_PROFILE, profile };
};

export const userLogout = (token, history) => {
	return { type: USER_LOGOUT, token, history };
};

export const userLogoutReceive = () => {
	return { type: USER_LOGOUT_RECEIVE };
};

export const userLogoutError = () => {
	return { type: USER_LOGOUT_ERROR };
};
