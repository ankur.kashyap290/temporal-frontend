// Imports: Dependencies
import { all, fork } from 'redux-saga/effects';
// Imports: Redux Sagas
import { userRegister, userLogin, userLogout } from './user/saga';
import {
	getAllDatabases,
	getDatabase,
	getTable,
	getTableData,
	createRecord,
	updateRecord,
	deleteRecord,
	runOperators,
} from './database/saga';

// Redux Saga: Root Saga
export function* rootSaga() {
	yield all([
		fork(userRegister),
		fork(userLogin),
		fork(userLogout),

		fork(getAllDatabases),
		fork(getDatabase),
		fork(getTable),
		fork(getTableData),
		fork(createRecord),
		fork(updateRecord),
		fork(deleteRecord),
		fork(runOperators),
	]);
}
