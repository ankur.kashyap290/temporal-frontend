// User Actions
export const USER_REGISTER = 'USER_REGISTER';
export const USER_REGISTER_RECEIVE = 'USER_REGISTER_RECEIVE';
export const USER_REGISTER_ERROR = 'USER_REGISTER_ERROR';
export const USER_LOGIN = 'USER_LOGIN';
export const USER_LOGIN_RECEIVE = 'USER_LOGIN_RECEIVE';
export const USER_LOGIN_ERROR = 'USER_LOGIN_ERROR';
export const RESET_PASSWORD = 'RESET_PASSWORD';
export const RESET_PASSWORD_RECEIVE = 'RESET_PASSWORD_RECEIVE';
export const RESET_PASSWORD_ERROR = 'RESET_PASSWORD_ERROR';
export const UPDATE_USER_DETAILS = 'UPDATE_USER_DETAILS';
export const UPDATE_USER_DETAILS_RECEIVE = 'UPDATE_USER_DETAILS_RECEIVE';
export const UPDATE_USER_DETAILS_ERROR = 'UPDATE_USER_DETAILS_ERROR';
export const SET_TOKEN = 'SET_TOKEN';
export const SET_PROFILE = 'SET_PROFILE';
export const USER_LOGOUT = 'USER_LOGOUT';
export const USER_LOGOUT_RECEIVE = 'USER_LOGOUT_RECEIVE';
export const USER_LOGOUT_ERROR = 'USER_LOGOUT_ERROR';
export const RESET_USER_DETAILS_UPDATED_FLAG =
	'RESET_USER_DETAILS_UPDATED_FLAG';
export const USER_PASSWORD_CHANGE = 'USER_PASSWORD_CHANGE';
export const USER_PASSWORD_CHANGE_RECEIVE = 'USER_PASSWORD_CHANGE_RECEIVE';
export const USER_PASSWORD_CHANGE_ERROR = 'USER_PASSWORD_CHANGE_ERROR';

// export const UPDATE_PROGRESS_NUMBER = 'UPDATE_PROGRESS_NUMBER';

// DATABASES Actions
export const GET_ALL_DATABASES = 'GET_ALL_DATABASES';
export const GET_ALL_DATABASES_RECEIVE = 'GET_ALL_DATABASES_RECEIVE';
export const GET_ALL_DATABASES_ERROR = 'GET_ALL_DATABASES_ERROR';
export const GET_DATABASE = 'GET_DATABASE';
export const GET_DATABASE_RECEIVE = 'GET_DATABASE_RECEIVE';
export const GET_DATABASE_ERROR = 'GET_DATABASE_ERROR';
export const GET_TABLE = 'GET_TABLE';
export const GET_TABLE_RECEIVE = 'GET_TABLE_RECEIVE';
export const GET_TABLE_ERROR = 'GET_TABLE_ERROR';
export const GET_TABLE_DATA = 'GET_TABLE_DATA';
export const GET_TABLE_DATA_RECEIVE = 'GET_TABLE_DATA_RECEIVE';
export const GET_TABLE_DATA_ERROR = 'GET_TABLE_DATA_ERROR';
export const CREATE_RECORD = 'CREATE_RECORD';
export const CREATE_RECORD_RECEIVE = 'CREATE_RECORD_RECEIVE';
export const CREATE_RECORD_ERROR = 'CREATE_RECORD_ERROR';
export const UPDATE_RECORD = 'UPDATE_RECORD';
export const UPDATE_RECORD_RECEIVE = 'UPDATE_RECORD_RECEIVE';
export const UPDATE_RECORD_ERROR = 'UPDATE_RECORD_ERROR';
export const DELETE_RECORD = 'DELETE_RECORD';
export const DELETE_RECORD_RECEIVE = 'DELETE_RECORD_RECEIVE';
export const DELETE_RECORD_ERROR = 'DELETE_RECORD_ERROR';
export const RUN_OPERATORS = 'RUN_OPERATORS';
export const RUN_OPERATORS_RECEIVE = 'RUN_OPERATORS_RECEIVE';
export const RUN_OPERATORS_ERROR = 'RUN_OPERATORS_ERROR';
export const RESET_FLAGS = 'RESET_FLAGS';
