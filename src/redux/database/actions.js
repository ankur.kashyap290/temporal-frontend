import {
	GET_ALL_DATABASES,
	GET_ALL_DATABASES_RECEIVE,
	GET_ALL_DATABASES_ERROR,
	GET_DATABASE,
	GET_DATABASE_RECEIVE,
	GET_DATABASE_ERROR,
	GET_TABLE,
	GET_TABLE_RECEIVE,
	GET_TABLE_ERROR,
	GET_TABLE_DATA,
	GET_TABLE_DATA_RECEIVE,
	GET_TABLE_DATA_ERROR,
	CREATE_RECORD,
	CREATE_RECORD_RECEIVE,
	CREATE_RECORD_ERROR,
	UPDATE_RECORD,
	UPDATE_RECORD_RECEIVE,
	UPDATE_RECORD_ERROR,
	DELETE_RECORD,
	DELETE_RECORD_RECEIVE,
	DELETE_RECORD_ERROR,
	RUN_OPERATORS,
	RUN_OPERATORS_RECEIVE,
	RUN_OPERATORS_ERROR,
	RESET_FLAGS,
} from '../actionTypes';

export const getAllDatabases = (token) => {
	return { type: GET_ALL_DATABASES, token };
};

export const getAllDatabasesReceive = (response) => {
	return { type: GET_ALL_DATABASES_RECEIVE, response };
};

export const getAllDatabasesError = (error) => {
	return { type: GET_ALL_DATABASES_ERROR, error };
};

export const getDatabase = (payload, token) => {
	return { type: GET_DATABASE, payload, token };
};

export const getDatabaseReceive = (response) => {
	return { type: GET_DATABASE_RECEIVE, response };
};

export const getDatabaseError = (error) => {
	return { type: GET_DATABASE_ERROR, error };
};

export const getTable = (payload, token) => {
	return { type: GET_TABLE, payload, token };
};

export const getTableReceive = (response) => {
	return { type: GET_TABLE_RECEIVE, response };
};

export const getTableError = (error) => {
	return { type: GET_TABLE_ERROR, error };
};

export const getTableData = (payload, token) => {
	return { type: GET_TABLE_DATA, payload, token };
};

export const getTableDataReceive = (response) => {
	return { type: GET_TABLE_DATA_RECEIVE, response };
};

export const getTableDataError = (error) => {
	return { type: GET_TABLE_DATA_ERROR, error };
};

export const createRecord = (payload, token) => {
	return { type: CREATE_RECORD, payload, token };
};

export const createRecordReceive = (response) => {
	return { type: CREATE_RECORD_RECEIVE, response };
};

export const createRecordError = (error) => {
	return { type: CREATE_RECORD_ERROR, error };
};

export const updateRecord = (payload, token) => {
	return { type: UPDATE_RECORD, payload, token };
};

export const updateRecordReceive = (response) => {
	return { type: UPDATE_RECORD_RECEIVE, response };
};

export const updateRecordError = (error) => {
	return { type: UPDATE_RECORD_ERROR, error };
};

export const deleteRecord = (payload, token) => {
	return { type: DELETE_RECORD, payload, token };
};

export const deleteRecordReceive = (response) => {
	return { type: DELETE_RECORD_RECEIVE, response };
};

export const deleteRecordError = (error) => {
	return { type: DELETE_RECORD_ERROR, error };
};
export const runOperators = (payload) => {
	return { type: RUN_OPERATORS, payload };
};

export const runOperatorsReceive = (response) => {
	return { type: RUN_OPERATORS_RECEIVE, response };
};

export const runOperatorsError = (error) => {
	return { type: RUN_OPERATORS_ERROR, error };
};
export const resetFlags = () => {
	return { type: RESET_FLAGS };
};
