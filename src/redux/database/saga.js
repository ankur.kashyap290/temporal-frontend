import { takeEvery, put, call } from 'redux-saga/effects';
import {
	GET_ALL_DATABASES,
	GET_DATABASE,
	GET_TABLE,
	GET_TABLE_DATA,
	CREATE_RECORD,
	UPDATE_RECORD,
	DELETE_RECORD,
	RUN_OPERATORS,
} from '../actionTypes';
import {
	getAllDatabasesReceive,
	getAllDatabasesError,
	getDatabaseReceive,
	getDatabaseError,
	getTableReceive,
	getTableError,
	getTableDataReceive,
	getTableDataError,
	createRecordReceive,
	createRecordError,
	updateRecordReceive,
	updateRecordError,
	deleteRecordReceive,
	deleteRecordError,
	runOperatorsReceive,
	runOperatorsError,
} from './actions';
import { databaseAPI } from '../../services/databaseAPI';

export function* getAllDatabases() {
	yield takeEvery(GET_ALL_DATABASES, function* ({ token }) {
		try {
			const response = yield call(databaseAPI.getAllDatabases, token);
			if (response.status.toLowerCase() === 'ok') {
				yield put(
					getAllDatabasesReceive({
						data: response.data.data,
					})
				);
			} else {
				yield put(getAllDatabasesError(response.message));
			}
		} catch (ex) {
			yield put(getAllDatabasesError('Error while getting request databases'));
		}
	});
}

export function* getDatabase() {
	yield takeEvery(GET_DATABASE, function* ({ payload, token }) {
		try {
			const response = yield call(databaseAPI.getDatabase, payload, token);
			if (response.status.toLowerCase() === 'ok') {
				yield put(getDatabaseReceive(response.data.data));
			} else {
				yield put(getDatabaseError(response.message));
			}
		} catch (ex) {
			yield put(
				getDatabaseError('Error while getting requested database details')
			);
		}
	});
}

export function* getTable() {
	yield takeEvery(GET_TABLE, function* ({ payload, token }) {
		try {
			const response = yield call(databaseAPI.getTable, payload, token);
			if (response.status.toLowerCase() === 'ok') {
				yield put(getTableReceive(response.data.data));
			} else {
				yield put(getTableError(response.message));
			}
		} catch (ex) {
			yield put(getTableError('Error while getting requested table details'));
		}
	});
}

export function* getTableData() {
	yield takeEvery(GET_TABLE_DATA, function* ({ payload, token }) {
		try {
			const response = yield call(databaseAPI.getTableData, payload, token);
			if (response.status.toLowerCase() === 'ok') {
				yield put(getTableDataReceive(response.data.data));
			} else {
				yield put(getTableDataError(response.message));
			}
		} catch (ex) {
			yield put(getTableDataError('Error while getting requested table data'));
		}
	});
}

export function* createRecord() {
	yield takeEvery(CREATE_RECORD, function* ({ payload, token }) {
		try {
			const response = yield call(databaseAPI.createRecord, payload, token);
			if (response.status.toLowerCase() === 'ok') {
				yield put(createRecordReceive(response.data.data));
			} else {
				yield put(createRecordError(response.message));
			}
		} catch (ex) {
			yield put(createRecordError('Error while creating record'));
		}
	});
}

export function* updateRecord() {
	yield takeEvery(UPDATE_RECORD, function* ({ payload, token }) {
		try {
			const response = yield call(databaseAPI.updateRecord, payload, token);
			if (response.status.toLowerCase() === 'ok') {
				yield put(updateRecordReceive(response.data.data));
			} else {
				yield put(updateRecordError(response.message));
			}
		} catch (ex) {
			yield put(updateRecordError('Error while updating record'));
		}
	});
}

export function* deleteRecord() {
	yield takeEvery(DELETE_RECORD, function* ({ payload, token }) {
		try {
			const response = yield call(databaseAPI.deleteRecord, payload, token);
			if (response.status.toLowerCase() === 'ok') {
				yield put(deleteRecordReceive(response.data.data));
			} else {
				yield put(deleteRecordError(response.message));
			}
		} catch (ex) {
			yield put(deleteRecordError('Error while deleting record'));
		}
	});
}

export function* runOperators() {
	yield takeEvery(RUN_OPERATORS, function* ({ payload, token }) {
		try {
			const response = yield call(databaseAPI.runOperators, payload, token);
			if (response.status.toLowerCase() === 'ok') {
				yield put(runOperatorsReceive(response.data.data));
			} else {
				yield put(runOperatorsError(response.message));
			}
		} catch (ex) {
			yield put(runOperatorsError('Error while fetching record'));
		}
	});
}
