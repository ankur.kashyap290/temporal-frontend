import {
	GET_ALL_DATABASES,
	GET_ALL_DATABASES_RECEIVE,
	GET_ALL_DATABASES_ERROR,
	GET_DATABASE,
	GET_DATABASE_RECEIVE,
	GET_DATABASE_ERROR,
	GET_TABLE,
	GET_TABLE_RECEIVE,
	GET_TABLE_ERROR,
	GET_TABLE_DATA,
	GET_TABLE_DATA_RECEIVE,
	GET_TABLE_DATA_ERROR,
	CREATE_RECORD,
	CREATE_RECORD_RECEIVE,
	CREATE_RECORD_ERROR,
	UPDATE_RECORD,
	UPDATE_RECORD_RECEIVE,
	UPDATE_RECORD_ERROR,
	DELETE_RECORD,
	DELETE_RECORD_RECEIVE,
	DELETE_RECORD_ERROR,
	RUN_OPERATORS,
	RUN_OPERATORS_RECEIVE,
	RUN_OPERATORS_ERROR,
	RESET_FLAGS,
} from '../actionTypes';

const initState = {
	loading: false,
	error: {},
	databases: [],
	tables: [],
	columns: [],
	tableData: [],
	isRecordCreated: false,
	isRecordUpdated: false,
	isRecordDeleted: false,
};

const databaseReducer = (state = initState, action = {}) => {
	switch (action.type) {
		case GET_ALL_DATABASES: {
			return {
				...state,
				loading: true,
				error: null,
			};
		}
		case GET_ALL_DATABASES_RECEIVE: {
			return {
				...state,
				loading: false,
				databases: action.response.data[0],
			};
		}
		case GET_ALL_DATABASES_ERROR: {
			return {
				...state,
				loading: false,
				error: action.error,
			};
		}
		case GET_DATABASE: {
			return {
				...state,
				loading: true,
				error: null,
			};
		}
		case GET_DATABASE_RECEIVE: {
			return {
				...state,
				loading: false,
				tables: action.response[0],
			};
		}
		case GET_DATABASE_ERROR: {
			return {
				...state,
				loading: false,
				error: action.error,
			};
		}
		case GET_TABLE: {
			return {
				...state,
				loading: true,
				error: null,
			};
		}
		case GET_TABLE_RECEIVE: {
			return {
				...state,
				loading: false,
				columns: action.response[0],
			};
		}
		case GET_TABLE_ERROR: {
			return {
				...state,
				loading: false,
				error: action.error,
			};
		}
		case GET_TABLE_DATA: {
			return {
				...state,
				loading: true,
				error: null,
			};
		}
		case GET_TABLE_DATA_RECEIVE: {
			return {
				...state,
				loading: false,
				tableData: action.response[0],
			};
		}
		case GET_TABLE_DATA_ERROR: {
			return {
				...state,
				loading: false,
				error: action.error,
			};
		}
		case CREATE_RECORD: {
			return {
				...state,
				loading: true,
				error: null,
			};
		}
		case CREATE_RECORD_RECEIVE: {
			return {
				...state,
				loading: false,
				tableData: action.response[0],
				isRecordCreated: true,
			};
		}
		case CREATE_RECORD_ERROR: {
			return {
				...state,
				loading: false,
				error: action.error,
			};
		}
		case UPDATE_RECORD: {
			return {
				...state,
				loading: true,
				error: null,
			};
		}
		case UPDATE_RECORD_RECEIVE: {
			return {
				...state,
				loading: false,
				tableData: action.response[0],
				isRecordUpdated: true,
			};
		}
		case UPDATE_RECORD_ERROR: {
			return {
				...state,
				loading: false,
				error: action.error,
			};
		}
		case DELETE_RECORD: {
			return {
				...state,
				loading: true,
				error: null,
			};
		}
		case DELETE_RECORD_RECEIVE: {
			return {
				...state,
				loading: false,
				tableData: action.response[0],
				isRecordDeleted: true,
			};
		}
		case DELETE_RECORD_ERROR: {
			return {
				...state,
				loading: false,
				error: action.error,
			};
		}
		case RUN_OPERATORS: {
			return {
				...state,
				loading: true,
				error: null,
			};
		}
		case RUN_OPERATORS_RECEIVE: {
			return {
				...state,
				loading: false,
			};
		}
		case RUN_OPERATORS_ERROR: {
			return {
				...state,
				loading: false,
				error: action.error,
			};
		}
		case RESET_FLAGS: {
			return {
				...state,
				isRecordCreated: false,
				isRecordUpdated: false,
				isRecordDeleted: false,
			};
		}
		default:
			return state;
	}
};

export default databaseReducer;
