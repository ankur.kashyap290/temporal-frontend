import React from 'react';
import { history } from './redux';
import AppRoutes from './pages';
import './App.css';

function App({}) {
	return <AppRoutes history={history} />;
}

export default App;
