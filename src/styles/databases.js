import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    quotationDescription:{
        padding:'0px 50px'
    }
}));

export default useStyles;
