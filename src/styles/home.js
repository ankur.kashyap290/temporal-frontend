import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
 noticeContainer:{
   backgroundColor:'#40bad2',
   minHeight:'130px',
   width:'100%'
 }
}));

export default useStyles;
