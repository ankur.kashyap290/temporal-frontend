import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    reviewsCard:{
        minHeight:'200px',
    },
    avatar: {
        height: "90px",
        width: "90px",
      },
      userName: {
        fontSize: theme.typography.pxToRem(20),
        color: "#474747",
        [theme.breakpoints.between("xs", "sm")]: {
          fontSize: theme.typography.pxToRem(16),
        },
      },
      reviewDate: {
        fontSize: theme.typography.pxToRem(12),
        color: "#ababab",
      },
      reviewDesc: {
        [theme.breakpoints.between("xs", "sm")]: {
          fontSize: theme.typography.pxToRem(16),
        },
      },
}));

export default useStyles;
