import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    imgBlock :{
        flex: "0 0 62.5%",
        width: '62.5%',
        paddingRight: '152px',
        zoom: 1,
        "&:before" :{
            content: ' ',
            display: "table",
        },
        "&:after" :{
          clear: "both",
          visibility: "hidden",
          fontSize: 0,
          height: 0,
        }
      },
      imgEle :{
        height: '360px',
        width: '100%',
        maxWidth: '430px',
        float: 'right',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: '50% 50%',
        backgroundSize: 'contain',
      },
      content: {
        flex: 'auto',
        h1: {
          color: '#434e59',
          fontSize: '72px',
          fontWeight: '600',
          lineHeight: '72px',
          marginBottom: '24px',
        },
        desc: {
          color:'red',
          fontSize: '20px',
          lineHeight: '28px',
          marginBottom: '16px'
        },
        actions: {
          'button:not(:last-child)': {
            marginRight: '8px',
          }
        }
      }
}));

export default useStyles;
