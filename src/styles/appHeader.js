import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  logo:{

  },
  sectionDesktop:{

  },
  appBar: {
    zIndex: theme.zIndex.drawer+1,
    backgroundColor: '#fff',
    color:theme.custom.black86,
    boxShadow: "0px 1px 0px -1px rgba(0,0,0,1), 0px 2px 10px 0px rgba(0,0,0,0.14), 0px 1px 1px 0px rgba(0,0,0,0)",
    position:'sticky'
  },
  drawer: {
    width: '20vw',
    flexShrink: 0,
  },
  drawerPaper: {
    width: '20vw',
    backgroundColor: '#fff',
    boxShadow: "0px 1px 0px -1px rgba(0,0,0,1), 0px 2px 10px 0px rgba(0,0,0,0.14), 0px 1px 1px 0px rgba(0,0,0,0)"
  },
  drawerContainer: {
    overflow: 'auto',
  },
}));

export default useStyles;
