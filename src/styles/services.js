import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  successModalHeading: {
    fontSize: theme.typography.pxToRem(24),
    color: theme.palette.secondary.main,
  },
  successModalConfirm: {
    fontSize: theme.typography.pxToRem(14),
    color: theme.custom.black60,
    textAlign:'center'
  },
  successContainer: {
    margin: "15px 50px",
    textAlign: "center",
    [theme.breakpoints.between("xs", "sm")]: {
      margin: "0px",
    },
  },
}));

export default useStyles;
