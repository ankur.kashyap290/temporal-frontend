import { makeStyles } from '@material-ui/core/styles';
import LoginBackground from '../assets/images/login-1.jpg';

const useStyles = makeStyles((theme) => ({
	formSection: {
		marginTop: '10vh',
	},
	signUpAndSignInHeading: {
		fontSize: theme.typography.pxToRem(25),
		margin: '30px 0px',
		textAlign: 'center',
		color: theme.palette.secondary.main,
	},
	inputFields: {
		borderRadius: '27px',
		width: '100%',
		marginBottom: '20px',
	},
	errorText: {
		color: '#ff0000',
	},
	inputBorder: {
		borderRadius: '30px',
		backgroundColor: '#fcfcfb',
	},
	showPassword: {
		color: theme.palette.primary.main,
	},
	checkbox: {
		margin: '20px 0px',
	},
	rememberMe: {
		color: '#b2b2b2',
		fontSize: theme.typography.pxToRem(12),
	},
	forgotLink: {
		marginBottom: '20px',
		fontSize: theme.typography.pxToRem(14),
		pointer: 'cursor',
		color: theme.palette.secondary.main,
	},
}));

export default useStyles;
