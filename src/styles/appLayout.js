import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  mainSection:{
    padding:'35px'
  },
  leftSection:{
    height:'92vh',
    backgroundColor:'#40bad2'
  },
  rightSection:{
    height:'92vh',
    border:'1px solid #000'
  },
  content: {
    flexGrow: 1,
    padding: '20px 20px 20px 22vw',
    height:`calc(100% - 64px)`,
    backgroundColor: theme.palette.background.default,
  }, 
}));

export default useStyles;
