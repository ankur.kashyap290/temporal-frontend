import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    senderMessage:{
        minHeight: '28px',
        backgroundColor: theme.palette.primary.main,
        width: "80%",
        borderRadius: "20px 20px 0px 20px",
        margin: "5px",
        padding: "14px",
        float: "right",
    },
    receiverMessage:{
        minHeight: '28px',
        backgroundColor: theme.palette.primary.main,
        width: "80%",
        borderRadius: "20px 20px 20px 0px",
        margin: "5px",
        padding: "14px",
        float: "left",
    },
    sendField:{
        height:`calc(100vh - 252px)`,
    },
    inputBorder: {
        borderRadius: "30px",
        backgroundColor: "#fcfcfb",
       width:'100%'
      },
      messagetime:{
          fontSize:'10px',
          margin:'0px 8px',
      },
      inputFields: {
        borderRadius: "27px",   
        margin: "10px",
      },
      lastMessagetime:{
        fontSize:'10px',
        marginBottom:'10px'
      }
}));

export default useStyles;
