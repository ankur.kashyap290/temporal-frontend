import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  mainCard: {
    backgroundColor: "#fff",
    borderRadius: "10px",
    margin: "20px",
    boxShadow: "0 0 5px #b2b2b2",
    [theme.breakpoints.between("xs", "sm")]: {
      margin: "0px",
      marginTop: "64px",
      borderRadius: "0px",
    },
  },
  headingText: {
    fontSize: theme.typography.pxToRem(26),
    color: theme.palette.secondary.main,
  },
  DescriptionText: {
    fontSize: theme.typography.pxToRem(14),
    color: "#474747",
    marginTop: "10px",
    marginBottom: "20px",
  },
  inputFields: {
    borderRadius: "27px",
    width: "95%",
    marginBottom: "20px",
  },
  inputLabels: {
    fontSize: theme.typography.pxToRem(12),
    textTransform: "uppercase",
    marginTop: "15px",
    marginBottom: "5px",
  },
  errorText: {
    color: "#ff0000",
    fontSize: theme.typography.pxToRem(10),
    marginLeft: "0px",
    marginRight: "0px",
  },
  inputBorder: {
    borderRadius: "30px",
    borderColor: "#61c1fc !important",
    backgroundColor: "#fcfcfb",
  },
}));

export default useStyles;
