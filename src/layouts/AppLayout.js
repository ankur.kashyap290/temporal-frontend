import React from "react";
// import {
//   Card, CardContent
// } from "@material-ui/core";
import AppHeader from '../components/AppHeader';
import useStyles from '../styles/appLayout';

const AppLayout = (props) => {
const classes= useStyles();
  return (
    <React.Fragment >
      <AppHeader {...props}/>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {/* <Card >
          <CardContent> */}
        {props.children}
        {/* </CardContent>
        </Card> */}
        
      </main>
    </React.Fragment>
  );
};

export default AppLayout;

