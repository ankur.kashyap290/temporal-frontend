const primaryColor = "#AEDADA";
const secondaryColor = '#006666'
const infoColor = "#FFBC00";
const successColor = "#54B948";
const dangerColor = "#FF0000";
const warningColor = "##FF8367";
const black100 = "#333333";
const black86 = "#4F4F4F";
const black60 = "#858585";
const black36 = "#B5B5B5";
const black16 = "#DEDEDE";
const black4 = "#F7F7F7";
const offwhite = "#F8FCFF";

const theme = {
  palette: {
    background: {
      default: offwhite,
    },
    primary: {
      // light: primaryColor[300],
      main: primaryColor,
      // dark: primaryColor[700],
      
    },
    secondary: {
      main: secondaryColor,
    },
    // error: will use the default color
    action: {
      // hover: "#f44336",
    },
  },
  typography: {
    h1: {
      fontFamily: "Poppins, sans-serif",
      color: black86,
      textAlign:'center'
    },
    h2: {
      fontFamily: "Poppins, sans-serif",
      color: black86,
      textAlign:'center'
    },
    h3: {
      fontFamily: "Poppins, sans-serif",
      color: black86,
      textAlign:'center'
    },
    h4: {
      fontFamily: "Poppins, sans-serif",
      color: black86,
      textAlign:'center'
    },
    h5: {
      fontFamily: "Poppins, sans-serif",
      color: black86,
      textAlign:'center'
    },
    h6: {
      fontFamily: "Poppins, sans-serif",
      color: black86,
      textAlign:'center'
    },
  },
  overrides: {
   
      link: {
        fontFamily: "Poppins, sans-serif",
        fontWeight: 300,
        fontSize: 15,
        color: black60,
        margin: "0 15px",
        textDecoration: "none",
        "&:hover": {
          color: primaryColor,
          fontWeight: 500,
        },
      },
      linkActive: {
        fontWeight: 500,
        color: primaryColor,
      },
    },
    MuiPaper: {
      rounded: {
        borderRadius: 16,
      },
    },
    MuiButton: {
      root: {
        fontFamily: "Poppins, sans-serif",
        fontSize: 14,
        padding: "2px 8px",
      },
      textSizeLarge: {
        fontSize: 16,
        fontWeight: 500,
      },
      textSizeSmall: {
        fontWeight: 500,
      },
      contained: {
        boxShadow: "none",
        borderRadius: 30,
        padding: "10px 16px",
      },
      outlined: {
        boxShadow: "none",
        borderRadius: 30,
        padding: "10px 16px",
      },
      containedSecondary: {
        borderRadius: 4,
        padding: "10px 16px",
      },
      outlinedSizeLarge: {
        fontSize: 18,
        fontWeight: 500,
        padding: "13px 22px",
      },
      outlinedSizeSmall: {
        fontSize: 14,
      },
      containedSizeLarge: {
        fontSize: 18,
        fontWeight: 500,
        padding: "13px 22px",
      },
      containedSizeSmall: {
        fontSize: 14,
      },
    },
    MuiContainer: {
      maxWidthLg: {
        padding: 0,
      },
    },
    MuiListItemIcon: {
      root: {
        minWidth: "30px",
      },
  },
  custom: {
    primaryColor,
    infoColor,
    successColor,
    dangerColor,
    warningColor,
    black100,
    black86,
    black60,
    black36,
    black16,
    black4,
    offwhite
  },
  
};

export default theme;
