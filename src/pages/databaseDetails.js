import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { bindActionCreators } from 'redux';
import { useParams } from 'react-router-dom';
import { Typography } from '@material-ui/core';
import AppLayout from '../layouts/AppLayout';
import CustomTable from '../components/Common/CustomTable';
import useStyles from '../styles/databases';
import { getDatabase } from '../redux/database/actions';
import { tablesHeadCells } from '../configs/app.config';

function createData(index, tableName, createdOn) {
	return { index, tableName, createdOn };
}
const DatabaseDetails = (props) => {
	const classes = useStyles();
	const { getDatabase, tables, token, history } = props;
	const params = useParams();

	useEffect(() => {
		// if (token) {
		if (params.name) {
			getDatabase(params.name, token);
		}
		// }
	}, [params.name]);

	const getRows = (tables) => {
		return tables.map((data, index) => {
			return createData(index + 1, data.TABLE_NAME, data.CREATE_TIME);
		});
	};

	return (
		<AppLayout {...props}>
			<Typography variant="h6" component="h6">
				List of all tables of{' '}
				{tables && tables.length && tables[0].TABLE_SCHEMA}
			</Typography>
			<div>
				<CustomTable
					history={history}
					rows={tables.length > 0 ? getRows(tables) : []}
					headCells={tablesHeadCells}
					pageType="tables"
					data={tables}
				/>
			</div>
		</AppLayout>
	);
};

const mapStateToProps = ({ database, user }) => {
	return { tables: database.tables, token: user.token };
};

const mapDispatchToProps = (dispatch) =>
	bindActionCreators(
		{
			getDatabase,
		},
		dispatch
	);

export default connect(mapStateToProps, mapDispatchToProps)(DatabaseDetails);
