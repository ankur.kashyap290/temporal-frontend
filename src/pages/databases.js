import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Typography } from '@material-ui/core';
import AppLayout from '../layouts/AppLayout';
import CustomTable from '../components/Common/CustomTable';
import useStyles from '../styles/databases';
import { getAllDatabases, getDatabase } from '../redux/database/actions';
import { databaseHeadCells } from '../configs/app.config';

function createData(index, database) {
	return { index, database };
}

const Databases = (props) => {
	const classes = useStyles();
	const { getAllDatabases, databases, history, token, getDatabase } = props;

	useEffect(() => {
		// if (token) {
		if (databases && databases.length === 0) {
			getAllDatabases(token);
		}
		// }
	}, []);

	const getRows = (databases) => {
		return databases.map((data, index) => {
			return createData(index + 1, data.Database);
		});
	};

	return (
		<AppLayout {...props}>
			<Typography variant="h6" component="h6">
				List of All Databases
			</Typography>
			<div>
				<CustomTable
					history={history}
					rows={databases.length > 0 ? getRows(databases) : []}
					headCells={databaseHeadCells}
					pageType="db"
					data={databases}
				/>
			</div>
		</AppLayout>
	);
};

const mapStateToProps = ({ database, user }) => {
	return { databases: database.databases, token: user.token };
};

const mapDispatchToProps = (dispatch) =>
	bindActionCreators(
		{
			getAllDatabases,
			getDatabase,
		},
		dispatch
	);

export default connect(mapStateToProps, mapDispatchToProps)(Databases);
