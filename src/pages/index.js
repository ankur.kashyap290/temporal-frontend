import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Login from './login';
import Home from './home';
import Databases from './databases';
import DatabaseDetails from './databaseDetails';
import TableDetails from './tableDetails';
import TableData from './tableData';
import Register from './register';
import SecuredRoute from './securedRoute';

const AppRoutes = ({ history }) => {
	return (
		<Router history={history}>
			<Switch>
				<Route exact path="/register" component={Register}></Route>
				<Route exact path="/login" component={Login}></Route>
				<SecuredRoute
					exact
					path="/databases"
					component={Databases}
					history={history}
				></SecuredRoute>
				<SecuredRoute
					exact
					path="/database/:name"
					component={DatabaseDetails}
					history={history}
				></SecuredRoute>
				<SecuredRoute
					exact
					path="/database/:databaseName/:tableName"
					component={TableDetails}
					history={history}
				></SecuredRoute>
				<SecuredRoute
					exact
					path="/tabledata/:databaseName/:tableName"
					component={TableData}
					history={history}
				></SecuredRoute>
				<SecuredRoute
					exact
					path="/"
					component={Home}
					history={history}
				></SecuredRoute>
			</Switch>
		</Router>
	);
};

export default AppRoutes;
