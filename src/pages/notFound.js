import React from "react";
import { Button } from "@material-ui/core";
import useStyles from "../styles/notFound";

const NotFound = ({ history }) => {
  const classes = useStyles();

  const handleHome = () => {
    history.push("/");
  };

  return (
    <div>
      <div className={classes.imgBlock}>
        <div
          className={classes.imgEle}
          // style={{ backgroundImage: `url(${img || exceptions[pageType].img})` }}
        />
      </div>
      <div className={classes.content}>
        <h1>404</h1>
        <div className={classes.desc}>Sorry! Page Not Found!</div>
        <div className={classes.actions}>
          <Button onClick={() => handleHome()}>Go To Home</Button>
        </div>
      </div>
    </div>
  );
};

export default NotFound;
