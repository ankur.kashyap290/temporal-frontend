import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { useParams } from 'react-router-dom';
import { Typography } from '@material-ui/core';
import AppLayout from '../layouts/AppLayout';
import CustomDataTable from '../components/Common/CustomDataTable';
import {
	getTableData,
	createRecord,
	updateRecord,
	deleteRecord,
	resetFlags,
	runOperators,
} from '../redux/database/actions';

function createData(index, columnName, columnType) {
	return { index, columnName, columnType };
}
const TableData = (props) => {
	const {
		getTableData,
		tableData,
		token,
		history,
		createRecord,
		updateRecord,
		deleteRecord,
		columns,
		resetFlags,
		isRecordCreated,
		isRecordUpdated,
		isRecordDeleted,
		runOperators,
	} = props;
	const params = useParams();

	useEffect(() => {
		// if (token) {
		if (params.databaseName || params.tableName) {
			getTableData(
				{
					databaseName: params.databaseName,
					tableName: params.tableName,
				},
				token
			);
		}
		// }
	}, [params.databaseName, params.tableName]);

	const getHeadCells = () => {
		const headCells = [];

		if (columns.length > 0) {
			columns.map((item) => {
				const temp = {
					id: item.COLUMN_NAME,
					type: item.COLUMN_TYPE,
					disablePadding: true,
					label: item.COLUMN_NAME,
				};
				headCells.push(temp);
			});
		}

		return headCells;
	};

	return (
		<AppLayout {...props}>
			<Typography variant="h6" component="h6">
				All data of {params && params.tableName} table of{' '}
				{params && params.databaseName} database
			</Typography>
			<div>
				<CustomDataTable
					history={history}
					rows={tableData.length > 0 ? tableData : []}
					headCells={getHeadCells()}
					pageType="tableData"
					createRecord={createRecord}
					updateRecord={updateRecord}
					deleteRecord={deleteRecord}
					params={params}
					resetFlags={resetFlags}
					isRecordCreated={isRecordCreated}
					isRecordUpdated={isRecordUpdated}
					isRecordDeleted={isRecordDeleted}
					runOperators={runOperators}
				/>
			</div>
		</AppLayout>
	);
};

const mapStateToProps = ({ database, user }) => {
	return {
		tableData: database.tableData,
		token: user.token,
		columns: database.columns,
		isRecordCreated: database.isRecordCreated,
		isRecordUpdated: database.isRecordUpdated,
		isRecordDeleted: database.isRecordDeleted,
	};
};

const mapDispatchToProps = (dispatch) =>
	bindActionCreators(
		{
			getTableData,
			createRecord,
			updateRecord,
			deleteRecord,
			resetFlags,
			runOperators,
		},
		dispatch
	);

export default connect(mapStateToProps, mapDispatchToProps)(TableData);
