import React, { useEffect } from 'react';
import { Typography } from '@material-ui/core';
import { connect } from 'react-redux';
import AppLayout from '../layouts/AppLayout';
import useStyles from '../styles/home';

const Home = (props) => {
	const classes = useStyles();

	return (
		<AppLayout {...props}>
			<Typography variant="h6" component="h6">
				Welcome!
			</Typography>
			<Typography variant="h6" component="h6" style={{ height: '80vh' }}>
				Ready to work on Temporal Databases
			</Typography>
		</AppLayout>
	);
};

const mapStateToProps = ({ user }) => {
	return {
		profile: user.profile,
		token: user.token,
		loading: user.loading,
	};
};

export default connect(mapStateToProps)(Home);
