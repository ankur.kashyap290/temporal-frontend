import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { bindActionCreators } from 'redux';
import { useParams } from 'react-router-dom';
import { Typography } from '@material-ui/core';
import AppLayout from '../layouts/AppLayout';
import CustomTable from '../components/Common/CustomTable';
import useStyles from '../styles/databases';
import { getTable } from '../redux/database/actions';
import { columnsHeadCells } from '../configs/app.config';

function createData(index, columnName, columnType) {
	return { index, columnName, columnType };
}
const TableDetails = (props) => {
	const classes = useStyles();
	const { getTable, columns, token, history } = props;
	const params = useParams();

	useEffect(() => {
		// if (token) {
		if (params.databaseName || params.tableName) {
			getTable(
				{ databaseName: params.databaseName, tableName: params.tableName },
				token
			);
		}
		// }
	}, [params.databaseName, params.tableName]);

	const getRows = (columns) => {
		return columns.map((data, index) => {
			return createData(index + 1, data.COLUMN_NAME, data.COLUMN_TYPE);
		});
	};

	return (
		<AppLayout {...props}>
			<Typography variant="h6" component="h6">
				List of all columns of{' '}
				{columns && columns.length && columns[0].TABLE_NAME}
			</Typography>
			<div>
				<CustomTable
					history={history}
					rows={columns.length > 0 ? getRows(columns) : []}
					headCells={columnsHeadCells}
					pageType="columns"
					data={columns}
				/>
			</div>
		</AppLayout>
	);
};

const mapStateToProps = ({ database, user }) => {
	return { columns: database.columns, token: user.token };
};

const mapDispatchToProps = (dispatch) =>
	bindActionCreators(
		{
			getTable,
		},
		dispatch
	);

export default connect(mapStateToProps, mapDispatchToProps)(TableDetails);
