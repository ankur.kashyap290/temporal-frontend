import React from 'react';
import { Grid } from '@material-ui/core';
import LoginForm from '../components/User/loginForm';
import useStyles from '../styles/login';

const Login = ({ history }) => {
	const classes = useStyles();

	return (
		<React.Fragment>
			<Grid container direction="row" justify="center" alignItems="center">
				<Grid item lg={6} md={6}>
					<div className={classes.formSection}>
						<LoginForm history={history} />
					</div>
				</Grid>
			</Grid>
		</React.Fragment>
	);
};

export default Login;
