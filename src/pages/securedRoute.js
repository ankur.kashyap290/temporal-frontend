import { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Route, Redirect } from 'react-router-dom';
import { setToken, setProfile } from '../redux/user/actions';
import { getProfile, getToken } from '../utils/tokenFunctions';

const SecuredRoute = (props) => {
	const { profile, token, component: Component, render, ...rest } = props;
	useEffect(() => {
		const token = getToken();
		const profile = getProfile();
		if (token) {
			setToken(token);
		}
		if (profile) {
			setProfile(profile);
		}
	}, [profile, token]);
	console.log(props);
	return profile && token ? (
		// <Component {...props} />
		<Route
			{...rest}
			render={(props) => (Component ? <Component {...props} /> : render(props))}
		/>
	) : (
		<Route {...rest} render={() => <Redirect to="/login" />} />

		// <Redirect
		// 	to={{
		// 		pathname: '/login',
		// 	}}
		// />
	);
};

const mapStateToProps = (state) => {
	return {
		profile: state.user.profile,
		token: state.user.token,
	};
};

const mapDispatchToProps = (dispatch) =>
	bindActionCreators(
		{
			setToken,
			setProfile,
		},
		dispatch
	);

export default connect(mapStateToProps, mapDispatchToProps)(SecuredRoute);
