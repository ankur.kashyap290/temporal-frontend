import React from 'react';
import { Grid } from '@material-ui/core';
import RegisterForm from '../components/User/registerForm';
import useStyles from '../styles/login';

const Register = ({ history }) => {
	const classes = useStyles();

	return (
		<React.Fragment>
			<Grid container direction="row" justify="center" alignItems="center">
				<Grid item lg={6} md={6}>
					<div className={classes.formSection}>
						<RegisterForm history={history} />
					</div>
				</Grid>
			</Grid>
		</React.Fragment>
	);
};

export default Register;
