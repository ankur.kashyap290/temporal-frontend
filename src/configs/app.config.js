export const tokenSalt = '$T-E-M-P-O-R-A-L$';

export const API_URL = 'http://localhost:8282';

export const userKey = '----TemporalUserKey@2021----';

export const sideNav = [
	{
		label: 'Databases',
		url: 'databases',
	},
];

export const databaseHeadCells = [
	{
		id: 'index',
		numeric: false,
		disablePadding: true,
		label: '',
	},
	{ id: 'database', numeric: false, disablePadding: false, label: 'Name' },
];

export const tablesHeadCells = [
	{
		id: 'index',
		numeric: false,
		disablePadding: true,
		label: '',
	},
	{
		id: 'tableName',
		numeric: false,
		disablePadding: false,
		label: 'Table Name',
	},
	{
		id: 'createdOn',
		numeric: false,
		disablePadding: false,
		label: 'created On',
	},
];

export const columnsHeadCells = [
	{
		id: 'index',
		numeric: false,
		disablePadding: true,
		label: '',
	},
	{
		id: 'columnName',
		numeric: false,
		disablePadding: false,
		label: 'Column Name',
	},
	{
		id: 'columnType',
		numeric: false,
		disablePadding: false,
		label: 'Column Type',
	},
];
